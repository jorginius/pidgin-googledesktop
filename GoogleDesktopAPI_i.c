

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Fri Aug 29 18:55:33 2008
 */
/* Compiler settings for GoogleDesktopAPI.idl:
    Oicf, W3, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level:   none 
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterIndexingPlugin,0x9CBE5894,0x03B1,0x48c9,0x92,0x2A,0xCE,0x5C,0x88,0x62,0x52,0xF3);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterCustomAction,0xC3173C82,0x8B78,0x4f08,0xB1,0x9A,0xA6,0x5B,0x7F,0x48,0x63,0x0A);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterDisplayPlugin,0x2c4f95b0,0xee5b,0x4d93,0x98,0xed,0xbb,0xd0,0xc2,0x91,0x39,0x76);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterDisplayPlugin2,0x918B59A2,0xB066,0x49d0,0xA4,0x5C,0x75,0x68,0x59,0x03,0x2A,0xE3);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterEventPlugin,0x6f995c91,0x0763,0x4ad0,0xa2,0x09,0x0b,0x4d,0x86,0x72,0x28,0x4b);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegisterQueryPlugin,0x4A2168E6,0x7128,0x453C,0x9F,0x0E,0xBE,0xE8,0x26,0x95,0x52,0x66);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopRegistrar,0xB7BC8A9B,0xDC73,0x42d4,0xAB,0x7D,0x17,0x17,0x86,0x19,0xC8,0xE4);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopIndexingComponentRegistration,0x151857B2,0x26E0,0x4f4d,0xAC,0xED,0x4F,0x7E,0x4B,0x20,0x65,0xEF);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopIndexingComponentRegister,0x9B311E80,0xBC95,0x4518,0xA5,0x8C,0x44,0x6E,0xC9,0xA0,0x82,0xB5);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopEvent,0xBDAC0047,0x4759,0x43a1,0xBA,0x04,0xB1,0x48,0xE1,0x67,0x9E,0x87);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopEvent2,0x79EDFDE2,0x6BC6,0x41BD,0xA5,0x4C,0xF8,0xAF,0xF2,0xF3,0x78,0x9A);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopNotifyEvent,0x1668C88E,0x4BBA,0x4569,0xAF,0xCD,0xDB,0x10,0x15,0xB6,0x51,0x9C);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopEventFactory,0xA79E51C6,0xDB2D,0x4a44,0x84,0x8E,0xA8,0xEB,0xB2,0x2E,0x53,0x37);


MIDL_DEFINE_GUID(IID, DIID_DGoogleDesktopFileNotify,0xA1DE6DB8,0xB20F,0x445c,0xBF,0xDE,0x16,0xC8,0xD5,0x3A,0x2F,0xA1);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopFilterCollection,0xFEF90C69,0x4A90,0x46be,0x9B,0x9E,0xC5,0x47,0xAA,0x10,0xF1,0x70);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopEventSubscription,0x9575DED8,0x9BA4,0x4a3b,0x83,0xAA,0x59,0xB2,0xCA,0xD0,0xCD,0xEF);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopEventPublisher,0xD7D23586,0x2724,0x4b05,0xAF,0x2D,0x67,0xB9,0x47,0x03,0xFE,0xEA);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopSchemaFilter,0xB7734D6E,0xC899,0x4322,0xB8,0x11,0xB9,0x73,0x07,0x1D,0x66,0x28);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopSchemaPropertyFilter,0x08A02699,0xA4BC,0x41a0,0xBF,0xEE,0xA5,0x83,0x95,0xED,0x22,0xA7);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopContextualFilter,0xFD7977E1,0x9FB8,0x4053,0xBF,0x8B,0xFE,0xAC,0xC2,0xEF,0xC4,0xC1);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopDocumentVersions,0x0519A8C8,0xEB41,0x48AF,0xAB,0x2D,0xA3,0xD5,0xBB,0x0C,0x7E,0x4C);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryResultItem,0x2E7C367B,0xF2F9,0x4FC2,0x81,0xEE,0x58,0xC3,0xBC,0xB2,0x1E,0xBB);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopItemPropertyIterator,0x116E9B38,0x4E9E,0x4f34,0xAC,0x8C,0xE8,0x2F,0xFD,0x28,0xBB,0xE8);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryResultSet,0x745C5E13,0x3B8D,0x43C3,0x9C,0x84,0x0B,0xD1,0xBE,0x5B,0x47,0x5B);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryResultSet2,0x01DC3495,0x5365,0x4a81,0xA2,0x44,0xEA,0x27,0x79,0x1B,0xAD,0x27);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQuery,0xED5535A3,0xA40D,0x4AE2,0x9F,0x88,0x37,0xA0,0xD1,0x5D,0xE7,0x09);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQuery2,0xD5D277C7,0xBC90,0x437f,0x81,0x51,0xE0,0x64,0x5B,0x6E,0x63,0x68);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryAPI,0xCE714D64,0x8BA7,0x4c0a,0xBC,0x7A,0xB9,0x24,0x97,0x31,0xE3,0xF6);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryAPI2,0x42761625,0xA427,0x4e24,0x90,0x11,0xB8,0x0A,0xC8,0x11,0x26,0x77);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopStatus,0x18954551,0xA102,0x4735,0x97,0x6A,0x25,0xDC,0x33,0xC9,0x3E,0x1F);


MIDL_DEFINE_GUID(IID, LIBID_GoogleDesktopAPILib,0x3D056FE7,0xEA8E,0x481a,0xB1,0x8F,0x0B,0x02,0xEB,0xF6,0xB3,0xC1);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopNotifyEvent2,0x4BD9C44A,0xEBB9,0x47ec,0x84,0x49,0x8D,0x77,0x7F,0x57,0x08,0xBE);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryResultItem2,0xCEA958A8,0x244C,0x49d7,0x81,0x9C,0x88,0x06,0x48,0xB3,0x2A,0x69);


MIDL_DEFINE_GUID(IID, IID_IGoogleDesktopQueryResultItem3,0x46FCE283,0x25D0,0x44f5,0x8E,0xF4,0x33,0xEC,0x53,0x28,0x32,0x7B);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktop,0x579822B3,0x44CD,0x4786,0x83,0xE0,0xAE,0x32,0xBC,0xB9,0xE6,0xB1);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopEventPublisher,0x2C6F11D4,0xCF22,0x4e1f,0xA2,0x71,0x2A,0x4A,0x03,0x93,0xAD,0xAC);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopIndexingComponentRegister,0xBB8B07A0,0xB8D1,0x44e0,0xA2,0x62,0xC9,0xB7,0x21,0x2A,0xEC,0x68);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopRegistrar,0xAC129136,0xEB1C,0x4fff,0xB0,0xA2,0x6D,0x67,0x61,0xBE,0x41,0x38);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopSchemaFilter,0x1B4C0C56,0x5990,0x4277,0x82,0x6E,0x15,0x08,0x03,0x7D,0xD8,0xA7);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopSchemaPropertyFilter,0x9130995A,0xB2F2,0x47c7,0xBD,0x60,0xBC,0x02,0xE9,0x50,0xA8,0xA8);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopContextualFilter,0x81ACBD30,0x3750,0x4c4e,0xBD,0xA1,0x17,0x3F,0xB5,0x09,0xD4,0x75);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopFilterCollection,0x2B62A832,0x2CA2,0x4843,0x86,0xCA,0x45,0x45,0x0D,0x35,0xEA,0xDA);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopQueryAPI,0x4E26495C,0xCA41,0x4085,0x96,0xC4,0x8C,0xA8,0x05,0xAF,0x29,0x7E);


MIDL_DEFINE_GUID(CLSID, CLSID_GoogleDesktopStatus,0x75B02994,0xEF97,0x421C,0xAA,0xCD,0x28,0xDA,0xC3,0x18,0x12,0x9B);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



