

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Fri Aug 29 18:55:33 2008
 */
/* Compiler settings for GoogleDesktopAPI.idl:
    Oicf, W3, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level:   none 
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __GoogleDesktopApi_h__
#define __GoogleDesktopApi_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterIndexingPlugin IGoogleDesktopRegisterIndexingPlugin;
#endif 	/* __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__
#define __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterCustomAction IGoogleDesktopRegisterCustomAction;
#endif 	/* __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterDisplayPlugin IGoogleDesktopRegisterDisplayPlugin;
#endif 	/* __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterDisplayPlugin2 IGoogleDesktopRegisterDisplayPlugin2;
#endif 	/* __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterEventPlugin IGoogleDesktopRegisterEventPlugin;
#endif 	/* __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterQueryPlugin IGoogleDesktopRegisterQueryPlugin;
#endif 	/* __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegistrar_FWD_DEFINED__
#define __IGoogleDesktopRegistrar_FWD_DEFINED__
typedef interface IGoogleDesktopRegistrar IGoogleDesktopRegistrar;
#endif 	/* __IGoogleDesktopRegistrar_FWD_DEFINED__ */


#ifndef __IGoogleDesktopIndexingComponentRegistration_FWD_DEFINED__
#define __IGoogleDesktopIndexingComponentRegistration_FWD_DEFINED__
typedef interface IGoogleDesktopIndexingComponentRegistration IGoogleDesktopIndexingComponentRegistration;
#endif 	/* __IGoogleDesktopIndexingComponentRegistration_FWD_DEFINED__ */


#ifndef __IGoogleDesktopIndexingComponentRegister_FWD_DEFINED__
#define __IGoogleDesktopIndexingComponentRegister_FWD_DEFINED__
typedef interface IGoogleDesktopIndexingComponentRegister IGoogleDesktopIndexingComponentRegister;
#endif 	/* __IGoogleDesktopIndexingComponentRegister_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEvent_FWD_DEFINED__
#define __IGoogleDesktopEvent_FWD_DEFINED__
typedef interface IGoogleDesktopEvent IGoogleDesktopEvent;
#endif 	/* __IGoogleDesktopEvent_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEvent2_FWD_DEFINED__
#define __IGoogleDesktopEvent2_FWD_DEFINED__
typedef interface IGoogleDesktopEvent2 IGoogleDesktopEvent2;
#endif 	/* __IGoogleDesktopEvent2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopNotifyEvent_FWD_DEFINED__
#define __IGoogleDesktopNotifyEvent_FWD_DEFINED__
typedef interface IGoogleDesktopNotifyEvent IGoogleDesktopNotifyEvent;
#endif 	/* __IGoogleDesktopNotifyEvent_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEventFactory_FWD_DEFINED__
#define __IGoogleDesktopEventFactory_FWD_DEFINED__
typedef interface IGoogleDesktopEventFactory IGoogleDesktopEventFactory;
#endif 	/* __IGoogleDesktopEventFactory_FWD_DEFINED__ */


#ifndef __DGoogleDesktopFileNotify_FWD_DEFINED__
#define __DGoogleDesktopFileNotify_FWD_DEFINED__
typedef interface DGoogleDesktopFileNotify DGoogleDesktopFileNotify;
#endif 	/* __DGoogleDesktopFileNotify_FWD_DEFINED__ */


#ifndef __IGoogleDesktopFilterCollection_FWD_DEFINED__
#define __IGoogleDesktopFilterCollection_FWD_DEFINED__
typedef interface IGoogleDesktopFilterCollection IGoogleDesktopFilterCollection;
#endif 	/* __IGoogleDesktopFilterCollection_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEventSubscription_FWD_DEFINED__
#define __IGoogleDesktopEventSubscription_FWD_DEFINED__
typedef interface IGoogleDesktopEventSubscription IGoogleDesktopEventSubscription;
#endif 	/* __IGoogleDesktopEventSubscription_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEventPublisher_FWD_DEFINED__
#define __IGoogleDesktopEventPublisher_FWD_DEFINED__
typedef interface IGoogleDesktopEventPublisher IGoogleDesktopEventPublisher;
#endif 	/* __IGoogleDesktopEventPublisher_FWD_DEFINED__ */


#ifndef __IGoogleDesktopSchemaFilter_FWD_DEFINED__
#define __IGoogleDesktopSchemaFilter_FWD_DEFINED__
typedef interface IGoogleDesktopSchemaFilter IGoogleDesktopSchemaFilter;
#endif 	/* __IGoogleDesktopSchemaFilter_FWD_DEFINED__ */


#ifndef __IGoogleDesktopSchemaPropertyFilter_FWD_DEFINED__
#define __IGoogleDesktopSchemaPropertyFilter_FWD_DEFINED__
typedef interface IGoogleDesktopSchemaPropertyFilter IGoogleDesktopSchemaPropertyFilter;
#endif 	/* __IGoogleDesktopSchemaPropertyFilter_FWD_DEFINED__ */


#ifndef __IGoogleDesktopContextualFilter_FWD_DEFINED__
#define __IGoogleDesktopContextualFilter_FWD_DEFINED__
typedef interface IGoogleDesktopContextualFilter IGoogleDesktopContextualFilter;
#endif 	/* __IGoogleDesktopContextualFilter_FWD_DEFINED__ */


#ifndef __IGoogleDesktopDocumentVersions_FWD_DEFINED__
#define __IGoogleDesktopDocumentVersions_FWD_DEFINED__
typedef interface IGoogleDesktopDocumentVersions IGoogleDesktopDocumentVersions;
#endif 	/* __IGoogleDesktopDocumentVersions_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem_FWD_DEFINED__
#define __IGoogleDesktopQueryResultItem_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultItem IGoogleDesktopQueryResultItem;
#endif 	/* __IGoogleDesktopQueryResultItem_FWD_DEFINED__ */


#ifndef __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__
#define __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__
typedef interface IGoogleDesktopItemPropertyIterator IGoogleDesktopItemPropertyIterator;
#endif 	/* __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultSet_FWD_DEFINED__
#define __IGoogleDesktopQueryResultSet_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultSet IGoogleDesktopQueryResultSet;
#endif 	/* __IGoogleDesktopQueryResultSet_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultSet2_FWD_DEFINED__
#define __IGoogleDesktopQueryResultSet2_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultSet2 IGoogleDesktopQueryResultSet2;
#endif 	/* __IGoogleDesktopQueryResultSet2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQuery_FWD_DEFINED__
#define __IGoogleDesktopQuery_FWD_DEFINED__
typedef interface IGoogleDesktopQuery IGoogleDesktopQuery;
#endif 	/* __IGoogleDesktopQuery_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQuery2_FWD_DEFINED__
#define __IGoogleDesktopQuery2_FWD_DEFINED__
typedef interface IGoogleDesktopQuery2 IGoogleDesktopQuery2;
#endif 	/* __IGoogleDesktopQuery2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI_FWD_DEFINED__
#define __IGoogleDesktopQueryAPI_FWD_DEFINED__
typedef interface IGoogleDesktopQueryAPI IGoogleDesktopQueryAPI;
#endif 	/* __IGoogleDesktopQueryAPI_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI2_FWD_DEFINED__
#define __IGoogleDesktopQueryAPI2_FWD_DEFINED__
typedef interface IGoogleDesktopQueryAPI2 IGoogleDesktopQueryAPI2;
#endif 	/* __IGoogleDesktopQueryAPI2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopStatus_FWD_DEFINED__
#define __IGoogleDesktopStatus_FWD_DEFINED__
typedef interface IGoogleDesktopStatus IGoogleDesktopStatus;
#endif 	/* __IGoogleDesktopStatus_FWD_DEFINED__ */


#ifndef __IGoogleDesktopNotifyEvent_FWD_DEFINED__
#define __IGoogleDesktopNotifyEvent_FWD_DEFINED__
typedef interface IGoogleDesktopNotifyEvent IGoogleDesktopNotifyEvent;
#endif 	/* __IGoogleDesktopNotifyEvent_FWD_DEFINED__ */


#ifndef __IGoogleDesktopNotifyEvent2_FWD_DEFINED__
#define __IGoogleDesktopNotifyEvent2_FWD_DEFINED__
typedef interface IGoogleDesktopNotifyEvent2 IGoogleDesktopNotifyEvent2;
#endif 	/* __IGoogleDesktopNotifyEvent2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopFilterCollection_FWD_DEFINED__
#define __IGoogleDesktopFilterCollection_FWD_DEFINED__
typedef interface IGoogleDesktopFilterCollection IGoogleDesktopFilterCollection;
#endif 	/* __IGoogleDesktopFilterCollection_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem_FWD_DEFINED__
#define __IGoogleDesktopQueryResultItem_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultItem IGoogleDesktopQueryResultItem;
#endif 	/* __IGoogleDesktopQueryResultItem_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem2_FWD_DEFINED__
#define __IGoogleDesktopQueryResultItem2_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultItem2 IGoogleDesktopQueryResultItem2;
#endif 	/* __IGoogleDesktopQueryResultItem2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem3_FWD_DEFINED__
#define __IGoogleDesktopQueryResultItem3_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultItem3 IGoogleDesktopQueryResultItem3;
#endif 	/* __IGoogleDesktopQueryResultItem3_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI_FWD_DEFINED__
#define __IGoogleDesktopQueryAPI_FWD_DEFINED__
typedef interface IGoogleDesktopQueryAPI IGoogleDesktopQueryAPI;
#endif 	/* __IGoogleDesktopQueryAPI_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEvent_FWD_DEFINED__
#define __IGoogleDesktopEvent_FWD_DEFINED__
typedef interface IGoogleDesktopEvent IGoogleDesktopEvent;
#endif 	/* __IGoogleDesktopEvent_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEvent2_FWD_DEFINED__
#define __IGoogleDesktopEvent2_FWD_DEFINED__
typedef interface IGoogleDesktopEvent2 IGoogleDesktopEvent2;
#endif 	/* __IGoogleDesktopEvent2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopEventSubscription_FWD_DEFINED__
#define __IGoogleDesktopEventSubscription_FWD_DEFINED__
typedef interface IGoogleDesktopEventSubscription IGoogleDesktopEventSubscription;
#endif 	/* __IGoogleDesktopEventSubscription_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterIndexingPlugin IGoogleDesktopRegisterIndexingPlugin;
#endif 	/* __IGoogleDesktopRegisterIndexingPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterEventPlugin IGoogleDesktopRegisterEventPlugin;
#endif 	/* __IGoogleDesktopRegisterEventPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__
#define __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterCustomAction IGoogleDesktopRegisterCustomAction;
#endif 	/* __IGoogleDesktopRegisterCustomAction_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterDisplayPlugin IGoogleDesktopRegisterDisplayPlugin;
#endif 	/* __IGoogleDesktopRegisterDisplayPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterDisplayPlugin2 IGoogleDesktopRegisterDisplayPlugin2;
#endif 	/* __IGoogleDesktopRegisterDisplayPlugin2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI2_FWD_DEFINED__
#define __IGoogleDesktopQueryAPI2_FWD_DEFINED__
typedef interface IGoogleDesktopQueryAPI2 IGoogleDesktopQueryAPI2;
#endif 	/* __IGoogleDesktopQueryAPI2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQuery_FWD_DEFINED__
#define __IGoogleDesktopQuery_FWD_DEFINED__
typedef interface IGoogleDesktopQuery IGoogleDesktopQuery;
#endif 	/* __IGoogleDesktopQuery_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQuery2_FWD_DEFINED__
#define __IGoogleDesktopQuery2_FWD_DEFINED__
typedef interface IGoogleDesktopQuery2 IGoogleDesktopQuery2;
#endif 	/* __IGoogleDesktopQuery2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultSet_FWD_DEFINED__
#define __IGoogleDesktopQueryResultSet_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultSet IGoogleDesktopQueryResultSet;
#endif 	/* __IGoogleDesktopQueryResultSet_FWD_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultSet2_FWD_DEFINED__
#define __IGoogleDesktopQueryResultSet2_FWD_DEFINED__
typedef interface IGoogleDesktopQueryResultSet2 IGoogleDesktopQueryResultSet2;
#endif 	/* __IGoogleDesktopQueryResultSet2_FWD_DEFINED__ */


#ifndef __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__
#define __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__
typedef interface IGoogleDesktopRegisterQueryPlugin IGoogleDesktopRegisterQueryPlugin;
#endif 	/* __IGoogleDesktopRegisterQueryPlugin_FWD_DEFINED__ */


#ifndef __IGoogleDesktopDocumentVersions_FWD_DEFINED__
#define __IGoogleDesktopDocumentVersions_FWD_DEFINED__
typedef interface IGoogleDesktopDocumentVersions IGoogleDesktopDocumentVersions;
#endif 	/* __IGoogleDesktopDocumentVersions_FWD_DEFINED__ */


#ifndef __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__
#define __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__
typedef interface IGoogleDesktopItemPropertyIterator IGoogleDesktopItemPropertyIterator;
#endif 	/* __IGoogleDesktopItemPropertyIterator_FWD_DEFINED__ */


#ifndef __IGoogleDesktopStatus_FWD_DEFINED__
#define __IGoogleDesktopStatus_FWD_DEFINED__
typedef interface IGoogleDesktopStatus IGoogleDesktopStatus;
#endif 	/* __IGoogleDesktopStatus_FWD_DEFINED__ */


#ifndef __GoogleDesktop_FWD_DEFINED__
#define __GoogleDesktop_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktop GoogleDesktop;
#else
typedef struct GoogleDesktop GoogleDesktop;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktop_FWD_DEFINED__ */


#ifndef __GoogleDesktopEventPublisher_FWD_DEFINED__
#define __GoogleDesktopEventPublisher_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopEventPublisher GoogleDesktopEventPublisher;
#else
typedef struct GoogleDesktopEventPublisher GoogleDesktopEventPublisher;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopEventPublisher_FWD_DEFINED__ */


#ifndef __GoogleDesktopIndexingComponentRegister_FWD_DEFINED__
#define __GoogleDesktopIndexingComponentRegister_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopIndexingComponentRegister GoogleDesktopIndexingComponentRegister;
#else
typedef struct GoogleDesktopIndexingComponentRegister GoogleDesktopIndexingComponentRegister;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopIndexingComponentRegister_FWD_DEFINED__ */


#ifndef __GoogleDesktopRegistrar_FWD_DEFINED__
#define __GoogleDesktopRegistrar_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopRegistrar GoogleDesktopRegistrar;
#else
typedef struct GoogleDesktopRegistrar GoogleDesktopRegistrar;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopRegistrar_FWD_DEFINED__ */


#ifndef __GoogleDesktopSchemaFilter_FWD_DEFINED__
#define __GoogleDesktopSchemaFilter_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopSchemaFilter GoogleDesktopSchemaFilter;
#else
typedef struct GoogleDesktopSchemaFilter GoogleDesktopSchemaFilter;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopSchemaFilter_FWD_DEFINED__ */


#ifndef __GoogleDesktopSchemaPropertyFilter_FWD_DEFINED__
#define __GoogleDesktopSchemaPropertyFilter_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopSchemaPropertyFilter GoogleDesktopSchemaPropertyFilter;
#else
typedef struct GoogleDesktopSchemaPropertyFilter GoogleDesktopSchemaPropertyFilter;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopSchemaPropertyFilter_FWD_DEFINED__ */


#ifndef __GoogleDesktopContextualFilter_FWD_DEFINED__
#define __GoogleDesktopContextualFilter_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopContextualFilter GoogleDesktopContextualFilter;
#else
typedef struct GoogleDesktopContextualFilter GoogleDesktopContextualFilter;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopContextualFilter_FWD_DEFINED__ */


#ifndef __GoogleDesktopFilterCollection_FWD_DEFINED__
#define __GoogleDesktopFilterCollection_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopFilterCollection GoogleDesktopFilterCollection;
#else
typedef struct GoogleDesktopFilterCollection GoogleDesktopFilterCollection;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopFilterCollection_FWD_DEFINED__ */


#ifndef __GoogleDesktopQueryAPI_FWD_DEFINED__
#define __GoogleDesktopQueryAPI_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopQueryAPI GoogleDesktopQueryAPI;
#else
typedef struct GoogleDesktopQueryAPI GoogleDesktopQueryAPI;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopQueryAPI_FWD_DEFINED__ */


#ifndef __GoogleDesktopStatus_FWD_DEFINED__
#define __GoogleDesktopStatus_FWD_DEFINED__

#ifdef __cplusplus
typedef class GoogleDesktopStatus GoogleDesktopStatus;
#else
typedef struct GoogleDesktopStatus GoogleDesktopStatus;
#endif /* __cplusplus */

#endif 	/* __GoogleDesktopStatus_FWD_DEFINED__ */


/* header files for imported files */
#include "OAIdl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_GoogleDesktopAPI_0000_0000 */
/* [local] */ 

#include <winerror.h>
#define E_EXTENSION_REGISTERED           MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0001)
#define E_COMPONENT_NOT_REGISTERED       MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0002)
#define E_NO_SUCH_SCHEMA                 MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0003)
#define E_NO_SUCH_PROPERTY               MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0004)
#define E_COMPONENT_DISABLED             MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0005)
#define E_COMPONENT_ALREADY_REGISTERED   MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0006)
#define S_INDEXING_PAUSED                MAKE_HRESULT(SEVERITY_SUCCESS, FACILITY_ITF, 0x0007)
#define E_EVENT_TOO_LARGE                MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0008)
#define E_SERVICE_NOT_RUNNING            MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0009)
#define E_INVALID_EVENT_FLAGS            MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x000A)
#define E_COMPONENT_PROHIBITED           MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x000B)
#define E_SEND_DELAYED                   MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x000C)
#define S_PROPERTY_TRUNCATED             MAKE_HRESULT(SEVERITY_SUCCESS, FACILITY_ITF, 0x000D)
#define E_PROPERTY_TOO_LARGE             MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x000E)
#define E_PROPERTY_NOT_SET               MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x000F)
#define E_SERVICE_IS_EXITING             MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0010)
#define E_APPLICATION_IS_EXITING         MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0011)
#define E_RETRY_SEND                     MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0012)
#define E_SEND_TIMEOUT                   MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0013)
#define E_REGISTRATION_CANCELLED_BY_USER MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0014)
#define E_SEARCH_LOCKED                  MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0015)
#define GDEF_FILTER_SCHEMA       L"GoogleDesktop.SchemaFilter"
#define GDEF_FILTER_PROPERTY     L"GoogleDesktop.SchemaPropertyFilter"
#define GDEF_FILTER_COLLECTION   L"GoogleDesktop.FilterCollection"
#define GDEF_FILTER_CONTEXTUAL   L"GoogleDesktop.ContextualFilter"
#define GD_SCHEMA_TEXTFILE       L"Google.Desktop.TextFile"
#define GD_SCHEMA_IM             L"Google.Desktop.IM"
#define GD_SCHEMA_EMAIL          L"Google.Desktop.Email"
#define GD_SCHEMA_WEBPAGE        L"Google.Desktop.WebPage"
#define GD_SCHEMA_MEDIAFILE      L"Google.Desktop.MediaFile"
#define GD_SCHEMA_FILE           L"Google.Desktop.File"
#define GD_SCHEMA_CONTACT        L"Google.Desktop.Contact"
#define GD_SCHEMA_CALENDAR       L"Google.Desktop.Calendar"
#define GD_SCHEMA_TASK           L"Google.Desktop.Task"
#define GD_SCHEMA_NOTE           L"Google.Desktop.Note"
#define GD_SCHEMA_JOURNAL        L"Google.Desktop.Journal"
#define GD_SCHEMA_CONTEXT_FOREGROUND_CHANGE L"Google.Desktop.Contextual.ForegroundChange"
#define GD_SCHEMA_CONTEXT_CONTEXT_UPDATE L"Google.Desktop.Contextual.ContextUpdate"
#define GD_SCHEMA_CONTEXT_SYSTEM_CHANGE L"Google.Desktop.Contextual.SystemChange"
typedef /* [uuid] */ 
enum GoogleDesktopDispId
    {	GD_DISPID_REGISTER_EXTENSION	= 1001,
	GD_DISPID_REGISTER_INDEXING_COMPONENT	= ( GD_DISPID_REGISTER_EXTENSION + 1 ) ,
	GD_DISPID_UNREGISTER_INDEXING_COMPONENT	= ( GD_DISPID_REGISTER_INDEXING_COMPONENT + 1 ) ,
	GD_DISPID_ADD_PROPERTY	= ( GD_DISPID_UNREGISTER_INDEXING_COMPONENT + 1 ) ,
	GD_DISPID_SEND	= ( GD_DISPID_ADD_PROPERTY + 1 ) ,
	GD_DISPID_SEND_EX	= ( GD_DISPID_SEND + 1 ) ,
	GD_DISPID_GET_PROPERTY	= ( GD_DISPID_SEND_EX + 1 ) ,
	GD_DISPID_SCHEMA_NAME	= ( GD_DISPID_GET_PROPERTY + 1 ) ,
	GD_DISPID_CREATE_EVENT	= ( GD_DISPID_SCHEMA_NAME + 1 ) ,
	GD_DISPID_SUBSCRIBE	= ( GD_DISPID_CREATE_EVENT + 1 ) ,
	GD_DISPID_UNSUBSCRIBE	= ( GD_DISPID_SUBSCRIBE + 1 ) ,
	GD_DISPID_ACTIVE	= ( GD_DISPID_UNSUBSCRIBE + 1 ) ,
	GD_DISPID_FILTER_OPERATOR	= ( GD_DISPID_ACTIVE + 1 ) ,
	GD_DISPID_FILTER_NEGATE	= ( GD_DISPID_FILTER_OPERATOR + 1 ) ,
	GD_DISPID_ADD_FILTER	= ( GD_DISPID_FILTER_NEGATE + 1 ) ,
	GD_DISPID_REMOVE_FILTER	= ( GD_DISPID_ADD_FILTER + 1 ) ,
	GD_DISPID_FIND_FILTER	= ( GD_DISPID_REMOVE_FILTER + 1 ) ,
	GD_DISPID_REMOVE_ALL_FILTERS	= ( GD_DISPID_FIND_FILTER + 1 ) ,
	GD_DISPID_ALLOW_NONE	= ( GD_DISPID_REMOVE_ALL_FILTERS + 1 ) ,
	GD_DISPID_ALLOW	= ( GD_DISPID_ALLOW_NONE + 1 ) ,
	GD_DISPID_DISALLOW	= ( GD_DISPID_ALLOW + 1 ) ,
	GD_DISPID_GET_ALLOWED	= ( GD_DISPID_DISALLOW + 1 ) ,
	GD_DISPID_ADD_REQUIRED	= ( GD_DISPID_GET_ALLOWED + 1 ) ,
	GD_DISPID_ADD_EXCLUDED	= ( GD_DISPID_ADD_REQUIRED + 1 ) ,
	GD_DISPID_REMOVE_REQUIRED	= ( GD_DISPID_ADD_EXCLUDED + 1 ) ,
	GD_DISPID_REMOVE_EXCLUDED	= ( GD_DISPID_REMOVE_REQUIRED + 1 ) ,
	GD_DISPID_GET_REQUIRED	= ( GD_DISPID_REMOVE_EXCLUDED + 1 ) ,
	GD_DISPID_GET_EXCLUDED	= ( GD_DISPID_GET_REQUIRED + 1 ) ,
	GD_DISPID_MATCH_WHOLE_WORD	= ( GD_DISPID_GET_EXCLUDED + 1 ) ,
	GD_DISPID_START_COMPONENT_REGISTRATION	= ( GD_DISPID_MATCH_WHOLE_WORD + 1 ) ,
	GD_DISPID_GET_REGISTRATION_INTERFACE	= ( GD_DISPID_START_COMPONENT_REGISTRATION + 1 ) ,
	GD_DISPID_FINISH_COMPONENT_REGISTRATION	= ( GD_DISPID_GET_REGISTRATION_INTERFACE + 1 ) ,
	GD_DISPID_UNREGISTER_COMPONENT	= ( GD_DISPID_FINISH_COMPONENT_REGISTRATION + 1 ) ,
	GD_DISPID_REGISTER_INDEXING_PLUGIN	= ( GD_DISPID_UNREGISTER_COMPONENT + 1 ) ,
	GD_DISPID_REGISTER_ACTION	= ( GD_DISPID_REGISTER_INDEXING_PLUGIN + 1 ) ,
	GD_DISPID_REGISTER_DISPLAY_PLUGIN	= ( GD_DISPID_REGISTER_ACTION + 1 ) ,
	GD_DISPID_REGISTER_EVENT_PLUGIN	= ( GD_DISPID_REGISTER_DISPLAY_PLUGIN + 1 ) ,
	GD_DISPID_REGISTER_QUERY_PLUGIN	= ( GD_DISPID_REGISTER_EVENT_PLUGIN + 1 ) ,
	GD_DISPID_Q_GET_PROPERTY	= ( GD_DISPID_REGISTER_QUERY_PLUGIN + 1 ) ,
	GD_DISPID_Q_NEXT	= ( GD_DISPID_Q_GET_PROPERTY + 1 ) ,
	GD_DISPID_Q_READY_STATE	= ( GD_DISPID_Q_NEXT + 1 ) ,
	GD_DISPID_Q_COUNT	= ( GD_DISPID_Q_READY_STATE + 1 ) ,
	GD_DISPID_Q_AVAILABLE_COUNT	= ( GD_DISPID_Q_COUNT + 1 ) ,
	GD_DISPID_Q_SET_OPTION	= ( GD_DISPID_Q_AVAILABLE_COUNT + 1 ) ,
	GD_DISPID_Q_GET_OPTION	= ( GD_DISPID_Q_SET_OPTION + 1 ) ,
	GD_DISPID_Q_QUERY_PROP	= ( GD_DISPID_Q_GET_OPTION + 1 ) ,
	GD_DISPID_Q_EXECUTE	= ( GD_DISPID_Q_QUERY_PROP + 1 ) ,
	GD_DISPID_Q_QUERY	= ( GD_DISPID_Q_EXECUTE + 1 ) ,
	GD_DISPID_Q_QUERY_EX	= ( GD_DISPID_Q_QUERY + 1 ) ,
	GD_DISPID_Q_REMOVE_FROM_INDEX	= ( GD_DISPID_Q_QUERY_EX + 1 ) ,
	GD_DISPID_Q_ALLOW_MSG_WHILE_WAIT	= ( GD_DISPID_Q_REMOVE_FROM_INDEX + 1 ) ,
	GD_DISPID_Q_CANCEL	= ( GD_DISPID_Q_ALLOW_MSG_WHILE_WAIT + 1 ) ,
	GD_DISPID_ENABLE_CONTEXTUAL	= ( GD_DISPID_Q_CANCEL + 1 ) ,
	GD_DISPID_ENABLE_CONTEXTUAL_ONLY	= ( GD_DISPID_ENABLE_CONTEXTUAL + 1 ) ,
	GD_DISPID_DISABLE_CONTEXTUAL	= ( GD_DISPID_ENABLE_CONTEXTUAL_ONLY + 1 ) ,
	GD_DISPID_GET_KEYWORDS	= ( GD_DISPID_DISABLE_CONTEXTUAL + 1 ) ,
	GD_DISPID_Q_GET_KEYWORDS	= ( GD_DISPID_GET_KEYWORDS + 1 ) ,
	GD_DISPID_Q_GET_SNIPPET	= ( GD_DISPID_Q_GET_KEYWORDS + 1 ) ,
	GD_DISPID_Q_QUERY_FOR_EVENT	= ( GD_DISPID_Q_GET_SNIPPET + 1 ) ,
	GD_DISPID_Q_ONKEYWORDSREADY	= ( GD_DISPID_Q_QUERY_FOR_EVENT + 1 ) ,
	GD_DISPID_Q_ONREADYSTATECHANGE	= ( GD_DISPID_Q_ONKEYWORDSREADY + 1 ) ,
	GD_DISPID_Q_ONAVAILABLE	= ( GD_DISPID_Q_ONREADYSTATECHANGE + 1 ) ,
	GD_DISPID_Q_ONCOUNTCHANGE	= ( GD_DISPID_Q_ONAVAILABLE + 1 ) ,
	GD_DISPID_Q_PREPARE_RESULT_SET	= ( GD_DISPID_Q_ONCOUNTCHANGE + 1 ) ,
	GD_DISPID_Q_OPEN	= ( GD_DISPID_Q_PREPARE_RESULT_SET + 1 ) ,
	GD_DISPID_Q_RESULT_SET_QUERY	= ( GD_DISPID_Q_OPEN + 1 ) ,
	GD_DISPID_STATUS_GET_PROPERTY	= ( GD_DISPID_Q_RESULT_SET_QUERY + 1 ) ,
	GD_DISPID_STATUS_SET_PROPERTY	= ( GD_DISPID_STATUS_GET_PROPERTY + 1 ) ,
	GD_DISPID_REGISTER_DISPLAY_EXTENSION	= ( GD_DISPID_STATUS_SET_PROPERTY + 1 ) 
    } 	GoogleDesktopDispId;

typedef /* [uuid] */ 
enum GoogleDesktopSubscriptionFilterOperator
    {	GD_FILTER_OPERATOR_AND	= 0,
	GD_FILTER_OPERATOR_OR	= ( GD_FILTER_OPERATOR_AND + 1 ) 
    } 	GoogleDesktopSubscriptionFilterOperator;



extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0000_v0_0_s_ifspec;

#ifndef __IGoogleDesktopRegisterIndexingPlugin_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterIndexingPlugin_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterIndexingPlugin */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterIndexingPlugin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterIndexingPlugin : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterIndexingPlugin( 
            BSTR extension_handled) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterIndexingPluginVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterIndexingPlugin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterIndexingPlugin * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterIndexingPlugin )( 
            IGoogleDesktopRegisterIndexingPlugin * This,
            BSTR extension_handled);
        
        END_INTERFACE
    } IGoogleDesktopRegisterIndexingPluginVtbl;

    interface IGoogleDesktopRegisterIndexingPlugin
    {
        CONST_VTBL struct IGoogleDesktopRegisterIndexingPluginVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterIndexingPlugin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterIndexingPlugin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterIndexingPlugin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterIndexingPlugin_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterIndexingPlugin_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterIndexingPlugin_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterIndexingPlugin_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterIndexingPlugin_RegisterIndexingPlugin(This,extension_handled)	\
    ( (This)->lpVtbl -> RegisterIndexingPlugin(This,extension_handled) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterIndexingPlugin_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegisterCustomAction_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterCustomAction_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterCustomAction */
/* [oleautomation][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterCustomAction;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterCustomAction : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterAction( 
            /* [in] */ BSTR action_class,
            /* [in] */ BSTR action_id,
            /* [in] */ VARIANT other_data) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterCustomActionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterCustomAction * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterCustomAction * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterAction )( 
            IGoogleDesktopRegisterCustomAction * This,
            /* [in] */ BSTR action_class,
            /* [in] */ BSTR action_id,
            /* [in] */ VARIANT other_data);
        
        END_INTERFACE
    } IGoogleDesktopRegisterCustomActionVtbl;

    interface IGoogleDesktopRegisterCustomAction
    {
        CONST_VTBL struct IGoogleDesktopRegisterCustomActionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterCustomAction_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterCustomAction_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterCustomAction_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterCustomAction_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterCustomAction_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterCustomAction_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterCustomAction_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterCustomAction_RegisterAction(This,action_class,action_id,other_data)	\
    ( (This)->lpVtbl -> RegisterAction(This,action_class,action_id,other_data) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterCustomAction_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterDisplayPlugin */
/* [oleautomation][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterDisplayPlugin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterDisplayPlugin : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterPlugin( 
            /* [in] */ BSTR plugin_class,
            /* [in] */ VARIANT_BOOL shows_notifications) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterDisplayPluginVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterDisplayPlugin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterDisplayPlugin * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterPlugin )( 
            IGoogleDesktopRegisterDisplayPlugin * This,
            /* [in] */ BSTR plugin_class,
            /* [in] */ VARIANT_BOOL shows_notifications);
        
        END_INTERFACE
    } IGoogleDesktopRegisterDisplayPluginVtbl;

    interface IGoogleDesktopRegisterDisplayPlugin
    {
        CONST_VTBL struct IGoogleDesktopRegisterDisplayPluginVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterDisplayPlugin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterDisplayPlugin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterDisplayPlugin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterDisplayPlugin_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterDisplayPlugin_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterDisplayPlugin_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterDisplayPlugin_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterDisplayPlugin_RegisterPlugin(This,plugin_class,shows_notifications)	\
    ( (This)->lpVtbl -> RegisterPlugin(This,plugin_class,shows_notifications) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterDisplayPlugin_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegisterDisplayPlugin2_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterDisplayPlugin2_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterDisplayPlugin2 */
/* [oleautomation][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterDisplayPlugin2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterDisplayPlugin2 : public IGoogleDesktopRegisterDisplayPlugin
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterExtension( 
            /* [in] */ BSTR ext_class,
            /* [in] */ VARIANT host_clsids) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterDisplayPlugin2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterPlugin )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ BSTR plugin_class,
            /* [in] */ VARIANT_BOOL shows_notifications);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterExtension )( 
            IGoogleDesktopRegisterDisplayPlugin2 * This,
            /* [in] */ BSTR ext_class,
            /* [in] */ VARIANT host_clsids);
        
        END_INTERFACE
    } IGoogleDesktopRegisterDisplayPlugin2Vtbl;

    interface IGoogleDesktopRegisterDisplayPlugin2
    {
        CONST_VTBL struct IGoogleDesktopRegisterDisplayPlugin2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterDisplayPlugin2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterDisplayPlugin2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterDisplayPlugin2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterDisplayPlugin2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterDisplayPlugin2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterDisplayPlugin2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterDisplayPlugin2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterDisplayPlugin2_RegisterPlugin(This,plugin_class,shows_notifications)	\
    ( (This)->lpVtbl -> RegisterPlugin(This,plugin_class,shows_notifications) ) 


#define IGoogleDesktopRegisterDisplayPlugin2_RegisterExtension(This,ext_class,host_clsids)	\
    ( (This)->lpVtbl -> RegisterExtension(This,ext_class,host_clsids) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterDisplayPlugin2_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegisterEventPlugin_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterEventPlugin_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterEventPlugin */
/* [oleautomation][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterEventPlugin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterEventPlugin : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterPlugin( 
            /* [in] */ BSTR plugin_class,
            /* [retval][out] */ long *cookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterEventPluginVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterEventPlugin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterEventPlugin * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterPlugin )( 
            IGoogleDesktopRegisterEventPlugin * This,
            /* [in] */ BSTR plugin_class,
            /* [retval][out] */ long *cookie);
        
        END_INTERFACE
    } IGoogleDesktopRegisterEventPluginVtbl;

    interface IGoogleDesktopRegisterEventPlugin
    {
        CONST_VTBL struct IGoogleDesktopRegisterEventPluginVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterEventPlugin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterEventPlugin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterEventPlugin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterEventPlugin_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterEventPlugin_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterEventPlugin_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterEventPlugin_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterEventPlugin_RegisterPlugin(This,plugin_class,cookie)	\
    ( (This)->lpVtbl -> RegisterPlugin(This,plugin_class,cookie) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterEventPlugin_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegisterQueryPlugin_INTERFACE_DEFINED__
#define __IGoogleDesktopRegisterQueryPlugin_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegisterQueryPlugin */
/* [oleautomation][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegisterQueryPlugin;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegisterQueryPlugin : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterPlugin( 
            /* [in] */ BSTR plugin_class,
            /* [in] */ VARIANT_BOOL read_only,
            /* [retval][out] */ long *cookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegisterQueryPluginVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegisterQueryPlugin * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegisterQueryPlugin * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterPlugin )( 
            IGoogleDesktopRegisterQueryPlugin * This,
            /* [in] */ BSTR plugin_class,
            /* [in] */ VARIANT_BOOL read_only,
            /* [retval][out] */ long *cookie);
        
        END_INTERFACE
    } IGoogleDesktopRegisterQueryPluginVtbl;

    interface IGoogleDesktopRegisterQueryPlugin
    {
        CONST_VTBL struct IGoogleDesktopRegisterQueryPluginVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegisterQueryPlugin_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegisterQueryPlugin_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegisterQueryPlugin_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegisterQueryPlugin_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegisterQueryPlugin_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegisterQueryPlugin_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegisterQueryPlugin_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegisterQueryPlugin_RegisterPlugin(This,plugin_class,read_only,cookie)	\
    ( (This)->lpVtbl -> RegisterPlugin(This,plugin_class,read_only,cookie) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegisterQueryPlugin_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopRegistrar_INTERFACE_DEFINED__
#define __IGoogleDesktopRegistrar_INTERFACE_DEFINED__

/* interface IGoogleDesktopRegistrar */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopRegistrar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopRegistrar : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE StartComponentRegistration( 
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ VARIANT component_description) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetRegistrationInterface( 
            /* [in] */ BSTR registration_type,
            /* [retval][out] */ IUnknown **registration_interface) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FinishComponentRegistration( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UnregisterComponent( 
            /* [in] */ BSTR component_guid_or_progid) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopRegistrarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopRegistrar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopRegistrar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopRegistrar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *StartComponentRegistration )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ VARIANT component_description);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetRegistrationInterface )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ BSTR registration_type,
            /* [retval][out] */ IUnknown **registration_interface);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FinishComponentRegistration )( 
            IGoogleDesktopRegistrar * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UnregisterComponent )( 
            IGoogleDesktopRegistrar * This,
            /* [in] */ BSTR component_guid_or_progid);
        
        END_INTERFACE
    } IGoogleDesktopRegistrarVtbl;

    interface IGoogleDesktopRegistrar
    {
        CONST_VTBL struct IGoogleDesktopRegistrarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopRegistrar_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopRegistrar_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopRegistrar_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopRegistrar_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopRegistrar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopRegistrar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopRegistrar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopRegistrar_StartComponentRegistration(This,component_guid_or_progid,component_description)	\
    ( (This)->lpVtbl -> StartComponentRegistration(This,component_guid_or_progid,component_description) ) 

#define IGoogleDesktopRegistrar_GetRegistrationInterface(This,registration_type,registration_interface)	\
    ( (This)->lpVtbl -> GetRegistrationInterface(This,registration_type,registration_interface) ) 

#define IGoogleDesktopRegistrar_FinishComponentRegistration(This)	\
    ( (This)->lpVtbl -> FinishComponentRegistration(This) ) 

#define IGoogleDesktopRegistrar_UnregisterComponent(This,component_guid_or_progid)	\
    ( (This)->lpVtbl -> UnregisterComponent(This,component_guid_or_progid) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopRegistrar_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopIndexingComponentRegistration_INTERFACE_DEFINED__
#define __IGoogleDesktopIndexingComponentRegistration_INTERFACE_DEFINED__

/* interface IGoogleDesktopIndexingComponentRegistration */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopIndexingComponentRegistration;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopIndexingComponentRegistration : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterExtension( 
            /* [in] */ BSTR extension_handled) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopIndexingComponentRegistrationVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopIndexingComponentRegistration * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopIndexingComponentRegistration * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterExtension )( 
            IGoogleDesktopIndexingComponentRegistration * This,
            /* [in] */ BSTR extension_handled);
        
        END_INTERFACE
    } IGoogleDesktopIndexingComponentRegistrationVtbl;

    interface IGoogleDesktopIndexingComponentRegistration
    {
        CONST_VTBL struct IGoogleDesktopIndexingComponentRegistrationVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopIndexingComponentRegistration_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopIndexingComponentRegistration_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopIndexingComponentRegistration_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopIndexingComponentRegistration_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopIndexingComponentRegistration_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopIndexingComponentRegistration_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopIndexingComponentRegistration_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopIndexingComponentRegistration_RegisterExtension(This,extension_handled)	\
    ( (This)->lpVtbl -> RegisterExtension(This,extension_handled) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopIndexingComponentRegistration_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopIndexingComponentRegister_INTERFACE_DEFINED__
#define __IGoogleDesktopIndexingComponentRegister_INTERFACE_DEFINED__

/* interface IGoogleDesktopIndexingComponentRegister */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopIndexingComponentRegister;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopIndexingComponentRegister : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegisterIndexingComponent( 
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ VARIANT component_description,
            /* [retval][out] */ IGoogleDesktopIndexingComponentRegistration **registration) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UnregisterIndexingComponent( 
            /* [in] */ BSTR component_guid_or_progid) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopIndexingComponentRegisterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopIndexingComponentRegister * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopIndexingComponentRegister * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegisterIndexingComponent )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ VARIANT component_description,
            /* [retval][out] */ IGoogleDesktopIndexingComponentRegistration **registration);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UnregisterIndexingComponent )( 
            IGoogleDesktopIndexingComponentRegister * This,
            /* [in] */ BSTR component_guid_or_progid);
        
        END_INTERFACE
    } IGoogleDesktopIndexingComponentRegisterVtbl;

    interface IGoogleDesktopIndexingComponentRegister
    {
        CONST_VTBL struct IGoogleDesktopIndexingComponentRegisterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopIndexingComponentRegister_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopIndexingComponentRegister_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopIndexingComponentRegister_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopIndexingComponentRegister_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopIndexingComponentRegister_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopIndexingComponentRegister_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopIndexingComponentRegister_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopIndexingComponentRegister_RegisterIndexingComponent(This,component_guid_or_progid,component_description,registration)	\
    ( (This)->lpVtbl -> RegisterIndexingComponent(This,component_guid_or_progid,component_description,registration) ) 

#define IGoogleDesktopIndexingComponentRegister_UnregisterIndexingComponent(This,component_guid_or_progid)	\
    ( (This)->lpVtbl -> UnregisterIndexingComponent(This,component_guid_or_progid) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopIndexingComponentRegister_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopEvent_INTERFACE_DEFINED__
#define __IGoogleDesktopEvent_INTERFACE_DEFINED__

/* interface IGoogleDesktopEvent */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopEvent;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopEvent : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddProperty( 
            BSTR property_name,
            VARIANT property_value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Send( 
            long event_flags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopEventVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopEvent * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopEvent * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopEvent * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopEvent * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopEvent * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopEvent * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopEvent * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddProperty )( 
            IGoogleDesktopEvent * This,
            BSTR property_name,
            VARIANT property_value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Send )( 
            IGoogleDesktopEvent * This,
            long event_flags);
        
        END_INTERFACE
    } IGoogleDesktopEventVtbl;

    interface IGoogleDesktopEvent
    {
        CONST_VTBL struct IGoogleDesktopEventVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopEvent_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopEvent_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopEvent_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopEvent_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopEvent_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopEvent_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopEvent_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopEvent_AddProperty(This,property_name,property_value)	\
    ( (This)->lpVtbl -> AddProperty(This,property_name,property_value) ) 

#define IGoogleDesktopEvent_Send(This,event_flags)	\
    ( (This)->lpVtbl -> Send(This,event_flags) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopEvent_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopEvent2_INTERFACE_DEFINED__
#define __IGoogleDesktopEvent2_INTERFACE_DEFINED__

/* interface IGoogleDesktopEvent2 */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopEvent2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopEvent2 : public IGoogleDesktopEvent
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SendEx( 
            /* [in] */ long event_flags,
            /* [retval][out] */ long *retry_in_millisec) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopEvent2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopEvent2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopEvent2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopEvent2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopEvent2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopEvent2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopEvent2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopEvent2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddProperty )( 
            IGoogleDesktopEvent2 * This,
            BSTR property_name,
            VARIANT property_value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Send )( 
            IGoogleDesktopEvent2 * This,
            long event_flags);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SendEx )( 
            IGoogleDesktopEvent2 * This,
            /* [in] */ long event_flags,
            /* [retval][out] */ long *retry_in_millisec);
        
        END_INTERFACE
    } IGoogleDesktopEvent2Vtbl;

    interface IGoogleDesktopEvent2
    {
        CONST_VTBL struct IGoogleDesktopEvent2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopEvent2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopEvent2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopEvent2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopEvent2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopEvent2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopEvent2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopEvent2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopEvent2_AddProperty(This,property_name,property_value)	\
    ( (This)->lpVtbl -> AddProperty(This,property_name,property_value) ) 

#define IGoogleDesktopEvent2_Send(This,event_flags)	\
    ( (This)->lpVtbl -> Send(This,event_flags) ) 


#define IGoogleDesktopEvent2_SendEx(This,event_flags,retry_in_millisec)	\
    ( (This)->lpVtbl -> SendEx(This,event_flags,retry_in_millisec) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopEvent2_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopNotifyEvent_INTERFACE_DEFINED__
#define __IGoogleDesktopNotifyEvent_INTERFACE_DEFINED__

/* interface IGoogleDesktopNotifyEvent */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopNotifyEvent;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopNotifyEvent : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetProperty( 
            /* [in] */ BSTR property_name,
            /* [retval][out] */ VARIANT *property_value) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_SchemaName( 
            /* [retval][out] */ BSTR *schema_name) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopNotifyEventVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopNotifyEvent * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopNotifyEvent * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopNotifyEvent * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopNotifyEvent * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopNotifyEvent * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopNotifyEvent * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopNotifyEvent * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopNotifyEvent * This,
            /* [in] */ BSTR property_name,
            /* [retval][out] */ VARIANT *property_value);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SchemaName )( 
            IGoogleDesktopNotifyEvent * This,
            /* [retval][out] */ BSTR *schema_name);
        
        END_INTERFACE
    } IGoogleDesktopNotifyEventVtbl;

    interface IGoogleDesktopNotifyEvent
    {
        CONST_VTBL struct IGoogleDesktopNotifyEventVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopNotifyEvent_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopNotifyEvent_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopNotifyEvent_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopNotifyEvent_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopNotifyEvent_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopNotifyEvent_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopNotifyEvent_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopNotifyEvent_GetProperty(This,property_name,property_value)	\
    ( (This)->lpVtbl -> GetProperty(This,property_name,property_value) ) 

#define IGoogleDesktopNotifyEvent_get_SchemaName(This,schema_name)	\
    ( (This)->lpVtbl -> get_SchemaName(This,schema_name) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopNotifyEvent_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopEventFactory_INTERFACE_DEFINED__
#define __IGoogleDesktopEventFactory_INTERFACE_DEFINED__

/* interface IGoogleDesktopEventFactory */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopEventFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopEventFactory : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateEvent( 
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ BSTR schema_name,
            /* [retval][out] */ IDispatch **event) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopEventFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopEventFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopEventFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopEventFactory * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopEventFactory * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopEventFactory * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopEventFactory * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopEventFactory * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateEvent )( 
            IGoogleDesktopEventFactory * This,
            /* [in] */ BSTR component_guid_or_progid,
            /* [in] */ BSTR schema_name,
            /* [retval][out] */ IDispatch **event);
        
        END_INTERFACE
    } IGoogleDesktopEventFactoryVtbl;

    interface IGoogleDesktopEventFactory
    {
        CONST_VTBL struct IGoogleDesktopEventFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopEventFactory_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopEventFactory_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopEventFactory_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopEventFactory_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopEventFactory_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopEventFactory_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopEventFactory_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopEventFactory_CreateEvent(This,component_guid_or_progid,schema_name,event)	\
    ( (This)->lpVtbl -> CreateEvent(This,component_guid_or_progid,schema_name,event) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopEventFactory_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopFilterCollection_INTERFACE_DEFINED__
#define __IGoogleDesktopFilterCollection_INTERFACE_DEFINED__

/* interface IGoogleDesktopFilterCollection */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopFilterCollection;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopFilterCollection : public IDispatch
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_filter_operator( 
            /* [retval][out] */ GoogleDesktopSubscriptionFilterOperator *op) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_filter_operator( 
            /* [in] */ GoogleDesktopSubscriptionFilterOperator op) = 0;
        
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_negate_result( 
            /* [retval][out] */ VARIANT_BOOL *not) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_negate_result( 
            /* [in] */ VARIANT_BOOL not) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddFilter( 
            /* [in] */ BSTR progid,
            /* [optional][in] */ VARIANT name,
            /* [retval][out] */ IDispatch **filter) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveFilter( 
            /* [in] */ IDispatch *filter) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindFilter( 
            /* [in] */ BSTR filter_name,
            /* [retval][out] */ IDispatch **filter) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAllFilters( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopFilterCollectionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopFilterCollection * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopFilterCollection * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopFilterCollection * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_filter_operator )( 
            IGoogleDesktopFilterCollection * This,
            /* [retval][out] */ GoogleDesktopSubscriptionFilterOperator *op);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_filter_operator )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ GoogleDesktopSubscriptionFilterOperator op);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_negate_result )( 
            IGoogleDesktopFilterCollection * This,
            /* [retval][out] */ VARIANT_BOOL *not);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_negate_result )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ VARIANT_BOOL not);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddFilter )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ BSTR progid,
            /* [optional][in] */ VARIANT name,
            /* [retval][out] */ IDispatch **filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFilter )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ IDispatch *filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindFilter )( 
            IGoogleDesktopFilterCollection * This,
            /* [in] */ BSTR filter_name,
            /* [retval][out] */ IDispatch **filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAllFilters )( 
            IGoogleDesktopFilterCollection * This);
        
        END_INTERFACE
    } IGoogleDesktopFilterCollectionVtbl;

    interface IGoogleDesktopFilterCollection
    {
        CONST_VTBL struct IGoogleDesktopFilterCollectionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopFilterCollection_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopFilterCollection_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopFilterCollection_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopFilterCollection_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopFilterCollection_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopFilterCollection_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopFilterCollection_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopFilterCollection_get_filter_operator(This,op)	\
    ( (This)->lpVtbl -> get_filter_operator(This,op) ) 

#define IGoogleDesktopFilterCollection_put_filter_operator(This,op)	\
    ( (This)->lpVtbl -> put_filter_operator(This,op) ) 

#define IGoogleDesktopFilterCollection_get_negate_result(This,not)	\
    ( (This)->lpVtbl -> get_negate_result(This,not) ) 

#define IGoogleDesktopFilterCollection_put_negate_result(This,not)	\
    ( (This)->lpVtbl -> put_negate_result(This,not) ) 

#define IGoogleDesktopFilterCollection_AddFilter(This,progid,name,filter)	\
    ( (This)->lpVtbl -> AddFilter(This,progid,name,filter) ) 

#define IGoogleDesktopFilterCollection_RemoveFilter(This,filter)	\
    ( (This)->lpVtbl -> RemoveFilter(This,filter) ) 

#define IGoogleDesktopFilterCollection_FindFilter(This,filter_name,filter)	\
    ( (This)->lpVtbl -> FindFilter(This,filter_name,filter) ) 

#define IGoogleDesktopFilterCollection_RemoveAllFilters(This)	\
    ( (This)->lpVtbl -> RemoveAllFilters(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopFilterCollection_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopEventSubscription_INTERFACE_DEFINED__
#define __IGoogleDesktopEventSubscription_INTERFACE_DEFINED__

/* interface IGoogleDesktopEventSubscription */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopEventSubscription;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopEventSubscription : public IGoogleDesktopFilterCollection
    {
    public:
        virtual /* [helpstring][propget][id] */ HRESULT STDMETHODCALLTYPE get_active( 
            /* [retval][out] */ VARIANT_BOOL *event_stream_enabled) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_active( 
            /* [in] */ VARIANT_BOOL event_stream_enabled) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopEventSubscriptionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopEventSubscription * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopEventSubscription * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopEventSubscription * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_filter_operator )( 
            IGoogleDesktopEventSubscription * This,
            /* [retval][out] */ GoogleDesktopSubscriptionFilterOperator *op);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_filter_operator )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ GoogleDesktopSubscriptionFilterOperator op);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_negate_result )( 
            IGoogleDesktopEventSubscription * This,
            /* [retval][out] */ VARIANT_BOOL *not);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_negate_result )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ VARIANT_BOOL not);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddFilter )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ BSTR progid,
            /* [optional][in] */ VARIANT name,
            /* [retval][out] */ IDispatch **filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFilter )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ IDispatch *filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindFilter )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ BSTR filter_name,
            /* [retval][out] */ IDispatch **filter);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAllFilters )( 
            IGoogleDesktopEventSubscription * This);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_active )( 
            IGoogleDesktopEventSubscription * This,
            /* [retval][out] */ VARIANT_BOOL *event_stream_enabled);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_active )( 
            IGoogleDesktopEventSubscription * This,
            /* [in] */ VARIANT_BOOL event_stream_enabled);
        
        END_INTERFACE
    } IGoogleDesktopEventSubscriptionVtbl;

    interface IGoogleDesktopEventSubscription
    {
        CONST_VTBL struct IGoogleDesktopEventSubscriptionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopEventSubscription_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopEventSubscription_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopEventSubscription_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopEventSubscription_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopEventSubscription_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopEventSubscription_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopEventSubscription_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopEventSubscription_get_filter_operator(This,op)	\
    ( (This)->lpVtbl -> get_filter_operator(This,op) ) 

#define IGoogleDesktopEventSubscription_put_filter_operator(This,op)	\
    ( (This)->lpVtbl -> put_filter_operator(This,op) ) 

#define IGoogleDesktopEventSubscription_get_negate_result(This,not)	\
    ( (This)->lpVtbl -> get_negate_result(This,not) ) 

#define IGoogleDesktopEventSubscription_put_negate_result(This,not)	\
    ( (This)->lpVtbl -> put_negate_result(This,not) ) 

#define IGoogleDesktopEventSubscription_AddFilter(This,progid,name,filter)	\
    ( (This)->lpVtbl -> AddFilter(This,progid,name,filter) ) 

#define IGoogleDesktopEventSubscription_RemoveFilter(This,filter)	\
    ( (This)->lpVtbl -> RemoveFilter(This,filter) ) 

#define IGoogleDesktopEventSubscription_FindFilter(This,filter_name,filter)	\
    ( (This)->lpVtbl -> FindFilter(This,filter_name,filter) ) 

#define IGoogleDesktopEventSubscription_RemoveAllFilters(This)	\
    ( (This)->lpVtbl -> RemoveAllFilters(This) ) 


#define IGoogleDesktopEventSubscription_get_active(This,event_stream_enabled)	\
    ( (This)->lpVtbl -> get_active(This,event_stream_enabled) ) 

#define IGoogleDesktopEventSubscription_put_active(This,event_stream_enabled)	\
    ( (This)->lpVtbl -> put_active(This,event_stream_enabled) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopEventSubscription_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopEventPublisher_INTERFACE_DEFINED__
#define __IGoogleDesktopEventPublisher_INTERFACE_DEFINED__

/* interface IGoogleDesktopEventPublisher */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopEventPublisher;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopEventPublisher : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Subscribe( 
            /* [in] */ long registration_cookie,
            /* [in] */ IUnknown *subscriber,
            /* [retval][out] */ IGoogleDesktopEventSubscription **subscription) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Unsubscribe( 
            /* [in] */ IGoogleDesktopEventSubscription *subscription) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopEventPublisherVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopEventPublisher * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopEventPublisher * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopEventPublisher * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Subscribe )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ long registration_cookie,
            /* [in] */ IUnknown *subscriber,
            /* [retval][out] */ IGoogleDesktopEventSubscription **subscription);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Unsubscribe )( 
            IGoogleDesktopEventPublisher * This,
            /* [in] */ IGoogleDesktopEventSubscription *subscription);
        
        END_INTERFACE
    } IGoogleDesktopEventPublisherVtbl;

    interface IGoogleDesktopEventPublisher
    {
        CONST_VTBL struct IGoogleDesktopEventPublisherVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopEventPublisher_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopEventPublisher_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopEventPublisher_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopEventPublisher_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopEventPublisher_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopEventPublisher_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopEventPublisher_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopEventPublisher_Subscribe(This,registration_cookie,subscriber,subscription)	\
    ( (This)->lpVtbl -> Subscribe(This,registration_cookie,subscriber,subscription) ) 

#define IGoogleDesktopEventPublisher_Unsubscribe(This,subscription)	\
    ( (This)->lpVtbl -> Unsubscribe(This,subscription) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopEventPublisher_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopSchemaFilter_INTERFACE_DEFINED__
#define __IGoogleDesktopSchemaFilter_INTERFACE_DEFINED__

/* interface IGoogleDesktopSchemaFilter */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopSchemaFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopSchemaFilter : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AllowNone( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Allow( 
            /* [in] */ BSTR schema_name) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Disallow( 
            /* [in] */ BSTR schema_name) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAllowed( 
            /* [retval][out] */ SAFEARRAY * *schema_names) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopSchemaFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopSchemaFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopSchemaFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopSchemaFilter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AllowNone )( 
            IGoogleDesktopSchemaFilter * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Allow )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ BSTR schema_name);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Disallow )( 
            IGoogleDesktopSchemaFilter * This,
            /* [in] */ BSTR schema_name);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAllowed )( 
            IGoogleDesktopSchemaFilter * This,
            /* [retval][out] */ SAFEARRAY * *schema_names);
        
        END_INTERFACE
    } IGoogleDesktopSchemaFilterVtbl;

    interface IGoogleDesktopSchemaFilter
    {
        CONST_VTBL struct IGoogleDesktopSchemaFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopSchemaFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopSchemaFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopSchemaFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopSchemaFilter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopSchemaFilter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopSchemaFilter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopSchemaFilter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopSchemaFilter_AllowNone(This)	\
    ( (This)->lpVtbl -> AllowNone(This) ) 

#define IGoogleDesktopSchemaFilter_Allow(This,schema_name)	\
    ( (This)->lpVtbl -> Allow(This,schema_name) ) 

#define IGoogleDesktopSchemaFilter_Disallow(This,schema_name)	\
    ( (This)->lpVtbl -> Disallow(This,schema_name) ) 

#define IGoogleDesktopSchemaFilter_GetAllowed(This,schema_names)	\
    ( (This)->lpVtbl -> GetAllowed(This,schema_names) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopSchemaFilter_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopSchemaPropertyFilter_INTERFACE_DEFINED__
#define __IGoogleDesktopSchemaPropertyFilter_INTERFACE_DEFINED__

/* interface IGoogleDesktopSchemaPropertyFilter */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopSchemaPropertyFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopSchemaPropertyFilter : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddRequiredText( 
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR required) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddExcludedText( 
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR excluded) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveRequiredText( 
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR required) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveExcludedText( 
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR excluded) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRequiredText( 
            /* [in] */ BSTR property_name,
            /* [retval][out] */ SAFEARRAY * *required) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetExcludedText( 
            /* [in] */ BSTR property_name,
            /* [retval][out] */ SAFEARRAY * *excluded) = 0;
        
        virtual /* [helpstring][propput][id] */ HRESULT STDMETHODCALLTYPE put_match_whole_word( 
            /* [in] */ VARIANT_BOOL whole) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopSchemaPropertyFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopSchemaPropertyFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopSchemaPropertyFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddRequiredText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR required);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddExcludedText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR excluded);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveRequiredText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR required);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveExcludedText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [in] */ BSTR excluded);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRequiredText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [retval][out] */ SAFEARRAY * *required);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetExcludedText )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ BSTR property_name,
            /* [retval][out] */ SAFEARRAY * *excluded);
        
        /* [helpstring][propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_match_whole_word )( 
            IGoogleDesktopSchemaPropertyFilter * This,
            /* [in] */ VARIANT_BOOL whole);
        
        END_INTERFACE
    } IGoogleDesktopSchemaPropertyFilterVtbl;

    interface IGoogleDesktopSchemaPropertyFilter
    {
        CONST_VTBL struct IGoogleDesktopSchemaPropertyFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopSchemaPropertyFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopSchemaPropertyFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopSchemaPropertyFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopSchemaPropertyFilter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopSchemaPropertyFilter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopSchemaPropertyFilter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopSchemaPropertyFilter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopSchemaPropertyFilter_AddRequiredText(This,property_name,required)	\
    ( (This)->lpVtbl -> AddRequiredText(This,property_name,required) ) 

#define IGoogleDesktopSchemaPropertyFilter_AddExcludedText(This,property_name,excluded)	\
    ( (This)->lpVtbl -> AddExcludedText(This,property_name,excluded) ) 

#define IGoogleDesktopSchemaPropertyFilter_RemoveRequiredText(This,property_name,required)	\
    ( (This)->lpVtbl -> RemoveRequiredText(This,property_name,required) ) 

#define IGoogleDesktopSchemaPropertyFilter_RemoveExcludedText(This,property_name,excluded)	\
    ( (This)->lpVtbl -> RemoveExcludedText(This,property_name,excluded) ) 

#define IGoogleDesktopSchemaPropertyFilter_GetRequiredText(This,property_name,required)	\
    ( (This)->lpVtbl -> GetRequiredText(This,property_name,required) ) 

#define IGoogleDesktopSchemaPropertyFilter_GetExcludedText(This,property_name,excluded)	\
    ( (This)->lpVtbl -> GetExcludedText(This,property_name,excluded) ) 

#define IGoogleDesktopSchemaPropertyFilter_put_match_whole_word(This,whole)	\
    ( (This)->lpVtbl -> put_match_whole_word(This,whole) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopSchemaPropertyFilter_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopContextualFilter_INTERFACE_DEFINED__
#define __IGoogleDesktopContextualFilter_INTERFACE_DEFINED__

/* interface IGoogleDesktopContextualFilter */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopContextualFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopContextualFilter : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnableContextualEvents( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnableContextualEventsOnly( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DisableContextualEvents( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopContextualFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopContextualFilter * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopContextualFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopContextualFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopContextualFilter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopContextualFilter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopContextualFilter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopContextualFilter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnableContextualEvents )( 
            IGoogleDesktopContextualFilter * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnableContextualEventsOnly )( 
            IGoogleDesktopContextualFilter * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DisableContextualEvents )( 
            IGoogleDesktopContextualFilter * This);
        
        END_INTERFACE
    } IGoogleDesktopContextualFilterVtbl;

    interface IGoogleDesktopContextualFilter
    {
        CONST_VTBL struct IGoogleDesktopContextualFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopContextualFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopContextualFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopContextualFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopContextualFilter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopContextualFilter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopContextualFilter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopContextualFilter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopContextualFilter_EnableContextualEvents(This)	\
    ( (This)->lpVtbl -> EnableContextualEvents(This) ) 

#define IGoogleDesktopContextualFilter_EnableContextualEventsOnly(This)	\
    ( (This)->lpVtbl -> EnableContextualEventsOnly(This) ) 

#define IGoogleDesktopContextualFilter_DisableContextualEvents(This)	\
    ( (This)->lpVtbl -> DisableContextualEvents(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopContextualFilter_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_GoogleDesktopAPI_0000_0019 */
/* [local] */ 









extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0019_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0019_v0_0_s_ifspec;

#ifndef __IGoogleDesktopDocumentVersions_INTERFACE_DEFINED__
#define __IGoogleDesktopDocumentVersions_INTERFACE_DEFINED__

/* interface IGoogleDesktopDocumentVersions */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopDocumentVersions;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopDocumentVersions : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Item( 
            /* [in] */ long index,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **ret) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_count( 
            /* [retval][out] */ long *ret) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopDocumentVersionsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopDocumentVersions * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopDocumentVersions * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopDocumentVersions * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopDocumentVersions * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopDocumentVersions * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopDocumentVersions * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopDocumentVersions * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Item )( 
            IGoogleDesktopDocumentVersions * This,
            /* [in] */ long index,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **ret);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_count )( 
            IGoogleDesktopDocumentVersions * This,
            /* [retval][out] */ long *ret);
        
        END_INTERFACE
    } IGoogleDesktopDocumentVersionsVtbl;

    interface IGoogleDesktopDocumentVersions
    {
        CONST_VTBL struct IGoogleDesktopDocumentVersionsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopDocumentVersions_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopDocumentVersions_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopDocumentVersions_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopDocumentVersions_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopDocumentVersions_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopDocumentVersions_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopDocumentVersions_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopDocumentVersions_Item(This,index,ret)	\
    ( (This)->lpVtbl -> Item(This,index,ret) ) 

#define IGoogleDesktopDocumentVersions_get_count(This,ret)	\
    ( (This)->lpVtbl -> get_count(This,ret) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopDocumentVersions_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryResultItem_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryResultItem */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryResultItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryResultItem : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetProperty( 
            /* [in] */ BSTR prop_name,
            /* [retval][out] */ VARIANT *value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveFromIndex( 
            /* [in] */ VARIANT_BOOL remove_all_related_versions) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_schema( 
            /* [retval][out] */ BSTR *schema_name) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_versions( 
            /* [retval][out] */ IGoogleDesktopDocumentVersions **ret) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryResultItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryResultItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryResultItem * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryResultItem * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ BSTR prop_name,
            /* [retval][out] */ VARIANT *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFromIndex )( 
            IGoogleDesktopQueryResultItem * This,
            /* [in] */ VARIANT_BOOL remove_all_related_versions);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_schema )( 
            IGoogleDesktopQueryResultItem * This,
            /* [retval][out] */ BSTR *schema_name);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_versions )( 
            IGoogleDesktopQueryResultItem * This,
            /* [retval][out] */ IGoogleDesktopDocumentVersions **ret);
        
        END_INTERFACE
    } IGoogleDesktopQueryResultItemVtbl;

    interface IGoogleDesktopQueryResultItem
    {
        CONST_VTBL struct IGoogleDesktopQueryResultItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryResultItem_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryResultItem_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryResultItem_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryResultItem_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryResultItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryResultItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryResultItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryResultItem_GetProperty(This,prop_name,value)	\
    ( (This)->lpVtbl -> GetProperty(This,prop_name,value) ) 

#define IGoogleDesktopQueryResultItem_RemoveFromIndex(This,remove_all_related_versions)	\
    ( (This)->lpVtbl -> RemoveFromIndex(This,remove_all_related_versions) ) 

#define IGoogleDesktopQueryResultItem_get_schema(This,schema_name)	\
    ( (This)->lpVtbl -> get_schema(This,schema_name) ) 

#define IGoogleDesktopQueryResultItem_get_versions(This,ret)	\
    ( (This)->lpVtbl -> get_versions(This,ret) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryResultItem_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopItemPropertyIterator_INTERFACE_DEFINED__
#define __IGoogleDesktopItemPropertyIterator_INTERFACE_DEFINED__

/* interface IGoogleDesktopItemPropertyIterator */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopItemPropertyIterator;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopItemPropertyIterator : public IDispatch
    {
    public:
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_name( 
            /* [retval][out] */ BSTR *name) = 0;
        
        virtual /* [propget] */ HRESULT STDMETHODCALLTYPE get_value( 
            /* [retval][out] */ VARIANT *value) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Next( 
            /* [retval][out] */ VARIANT_BOOL *more) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopItemPropertyIteratorVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopItemPropertyIterator * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopItemPropertyIterator * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_name )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [retval][out] */ BSTR *name);
        
        /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_value )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [retval][out] */ VARIANT *value);
        
        HRESULT ( STDMETHODCALLTYPE *Next )( 
            IGoogleDesktopItemPropertyIterator * This,
            /* [retval][out] */ VARIANT_BOOL *more);
        
        END_INTERFACE
    } IGoogleDesktopItemPropertyIteratorVtbl;

    interface IGoogleDesktopItemPropertyIterator
    {
        CONST_VTBL struct IGoogleDesktopItemPropertyIteratorVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopItemPropertyIterator_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopItemPropertyIterator_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopItemPropertyIterator_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopItemPropertyIterator_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopItemPropertyIterator_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopItemPropertyIterator_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopItemPropertyIterator_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopItemPropertyIterator_get_name(This,name)	\
    ( (This)->lpVtbl -> get_name(This,name) ) 

#define IGoogleDesktopItemPropertyIterator_get_value(This,value)	\
    ( (This)->lpVtbl -> get_value(This,value) ) 

#define IGoogleDesktopItemPropertyIterator_Next(This,more)	\
    ( (This)->lpVtbl -> Next(This,more) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopItemPropertyIterator_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_GoogleDesktopAPI_0000_0022 */
/* [local] */ 

typedef /* [uuid] */ 
enum QRSReadyState
    {	GD_QRS_UNINITIALIZED	= 0,
	GD_QRS_LOADING	= ( GD_QRS_UNINITIALIZED + 1 ) ,
	GD_QRS_COMPLETE	= ( GD_QRS_LOADING + 1 ) ,
	GD_QRS_FAILED	= ( GD_QRS_COMPLETE + 1 ) 
    } 	QRSReadyState;



extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0022_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0022_v0_0_s_ifspec;

#ifndef __IGoogleDesktopQueryResultSet_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryResultSet_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryResultSet */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryResultSet;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryResultSet : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [retval][out] */ IGoogleDesktopQueryResultItem **next) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_readystate( 
            /* [retval][out] */ QRSReadyState *state) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_count( 
            /* [retval][out] */ long *ret) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_available_count( 
            /* [retval][out] */ long *ret) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_enable_msg_loop( 
            /* [retval][out] */ VARIANT_BOOL *enable) = 0;
        
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_enable_msg_loop( 
            /* [in] */ VARIANT_BOOL enable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Cancel( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryResultSetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryResultSet * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryResultSet * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryResultSet * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryResultSet * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryResultSet * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryResultSet * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryResultSet * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IGoogleDesktopQueryResultSet * This,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **next);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_readystate )( 
            IGoogleDesktopQueryResultSet * This,
            /* [retval][out] */ QRSReadyState *state);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_count )( 
            IGoogleDesktopQueryResultSet * This,
            /* [retval][out] */ long *ret);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_available_count )( 
            IGoogleDesktopQueryResultSet * This,
            /* [retval][out] */ long *ret);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_enable_msg_loop )( 
            IGoogleDesktopQueryResultSet * This,
            /* [retval][out] */ VARIANT_BOOL *enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_enable_msg_loop )( 
            IGoogleDesktopQueryResultSet * This,
            /* [in] */ VARIANT_BOOL enable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            IGoogleDesktopQueryResultSet * This);
        
        END_INTERFACE
    } IGoogleDesktopQueryResultSetVtbl;

    interface IGoogleDesktopQueryResultSet
    {
        CONST_VTBL struct IGoogleDesktopQueryResultSetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryResultSet_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryResultSet_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryResultSet_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryResultSet_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryResultSet_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryResultSet_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryResultSet_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryResultSet_Next(This,next)	\
    ( (This)->lpVtbl -> Next(This,next) ) 

#define IGoogleDesktopQueryResultSet_get_readystate(This,state)	\
    ( (This)->lpVtbl -> get_readystate(This,state) ) 

#define IGoogleDesktopQueryResultSet_get_count(This,ret)	\
    ( (This)->lpVtbl -> get_count(This,ret) ) 

#define IGoogleDesktopQueryResultSet_get_available_count(This,ret)	\
    ( (This)->lpVtbl -> get_available_count(This,ret) ) 

#define IGoogleDesktopQueryResultSet_get_enable_msg_loop(This,enable)	\
    ( (This)->lpVtbl -> get_enable_msg_loop(This,enable) ) 

#define IGoogleDesktopQueryResultSet_put_enable_msg_loop(This,enable)	\
    ( (This)->lpVtbl -> put_enable_msg_loop(This,enable) ) 

#define IGoogleDesktopQueryResultSet_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryResultSet_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultSet2_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryResultSet2_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryResultSet2 */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryResultSet2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryResultSet2 : public IGoogleDesktopQueryResultSet
    {
    public:
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_onreadystatechange( 
            /* [in] */ VARIANT handler) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_onavailable( 
            /* [in] */ VARIANT handler) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_oncountchange( 
            /* [in] */ VARIANT handler) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_query( 
            /* [retval][out] */ BSTR *query) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryResultSet2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryResultSet2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryResultSet2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **next);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_readystate )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ QRSReadyState *state);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_count )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ long *ret);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_available_count )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ long *ret);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_enable_msg_loop )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ VARIANT_BOOL *enable);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_enable_msg_loop )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ VARIANT_BOOL enable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Cancel )( 
            IGoogleDesktopQueryResultSet2 * This);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_onreadystatechange )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ VARIANT handler);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_onavailable )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ VARIANT handler);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_oncountchange )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [in] */ VARIANT handler);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_query )( 
            IGoogleDesktopQueryResultSet2 * This,
            /* [retval][out] */ BSTR *query);
        
        END_INTERFACE
    } IGoogleDesktopQueryResultSet2Vtbl;

    interface IGoogleDesktopQueryResultSet2
    {
        CONST_VTBL struct IGoogleDesktopQueryResultSet2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryResultSet2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryResultSet2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryResultSet2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryResultSet2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryResultSet2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryResultSet2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryResultSet2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryResultSet2_Next(This,next)	\
    ( (This)->lpVtbl -> Next(This,next) ) 

#define IGoogleDesktopQueryResultSet2_get_readystate(This,state)	\
    ( (This)->lpVtbl -> get_readystate(This,state) ) 

#define IGoogleDesktopQueryResultSet2_get_count(This,ret)	\
    ( (This)->lpVtbl -> get_count(This,ret) ) 

#define IGoogleDesktopQueryResultSet2_get_available_count(This,ret)	\
    ( (This)->lpVtbl -> get_available_count(This,ret) ) 

#define IGoogleDesktopQueryResultSet2_get_enable_msg_loop(This,enable)	\
    ( (This)->lpVtbl -> get_enable_msg_loop(This,enable) ) 

#define IGoogleDesktopQueryResultSet2_put_enable_msg_loop(This,enable)	\
    ( (This)->lpVtbl -> put_enable_msg_loop(This,enable) ) 

#define IGoogleDesktopQueryResultSet2_Cancel(This)	\
    ( (This)->lpVtbl -> Cancel(This) ) 


#define IGoogleDesktopQueryResultSet2_put_onreadystatechange(This,handler)	\
    ( (This)->lpVtbl -> put_onreadystatechange(This,handler) ) 

#define IGoogleDesktopQueryResultSet2_put_onavailable(This,handler)	\
    ( (This)->lpVtbl -> put_onavailable(This,handler) ) 

#define IGoogleDesktopQueryResultSet2_put_oncountchange(This,handler)	\
    ( (This)->lpVtbl -> put_oncountchange(This,handler) ) 

#define IGoogleDesktopQueryResultSet2_get_query(This,query)	\
    ( (This)->lpVtbl -> get_query(This,query) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryResultSet2_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_GoogleDesktopAPI_0000_0024 */
/* [local] */ 

typedef 
enum GDQErrorMode
    {	GDQEM_REPORT_ERRORS	= 0,
	GDQEM_REPORT_SUCCESS_WHEN_CAN_RETURN_NULL	= ( GDQEM_REPORT_ERRORS + 1 ) ,
	GDQEM_DEFAULT	= GDQEM_REPORT_ERRORS,
	GDQEM_MAX	= GDQEM_REPORT_SUCCESS_WHEN_CAN_RETURN_NULL
    } 	GDQErrorMode;



extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0024_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0024_v0_0_s_ifspec;

#ifndef __IGoogleDesktopQuery_INTERFACE_DEFINED__
#define __IGoogleDesktopQuery_INTERFACE_DEFINED__

/* interface IGoogleDesktopQuery */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQuery;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQuery : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetOption( 
            /* [in] */ BSTR name,
            /* [in] */ VARIANT value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetOption( 
            /* [in] */ BSTR name,
            /* [retval][out] */ VARIANT *value) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_query( 
            /* [retval][out] */ BSTR *ret) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Execute( 
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQuery * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQuery * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQuery * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQuery * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQuery * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQuery * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQuery * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetOption )( 
            IGoogleDesktopQuery * This,
            /* [in] */ BSTR name,
            /* [in] */ VARIANT value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetOption )( 
            IGoogleDesktopQuery * This,
            /* [in] */ BSTR name,
            /* [retval][out] */ VARIANT *value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_query )( 
            IGoogleDesktopQuery * This,
            /* [retval][out] */ BSTR *ret);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Execute )( 
            IGoogleDesktopQuery * This,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results);
        
        END_INTERFACE
    } IGoogleDesktopQueryVtbl;

    interface IGoogleDesktopQuery
    {
        CONST_VTBL struct IGoogleDesktopQueryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQuery_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQuery_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQuery_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQuery_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQuery_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQuery_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQuery_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQuery_SetOption(This,name,value)	\
    ( (This)->lpVtbl -> SetOption(This,name,value) ) 

#define IGoogleDesktopQuery_GetOption(This,name,value)	\
    ( (This)->lpVtbl -> GetOption(This,name,value) ) 

#define IGoogleDesktopQuery_get_query(This,ret)	\
    ( (This)->lpVtbl -> get_query(This,ret) ) 

#define IGoogleDesktopQuery_Execute(This,results)	\
    ( (This)->lpVtbl -> Execute(This,results) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQuery_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQuery2_INTERFACE_DEFINED__
#define __IGoogleDesktopQuery2_INTERFACE_DEFINED__

/* interface IGoogleDesktopQuery2 */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQuery2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQuery2 : public IGoogleDesktopQuery
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE PrepareResultSet( 
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQuery2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQuery2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQuery2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQuery2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetOption )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ BSTR name,
            /* [in] */ VARIANT value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetOption )( 
            IGoogleDesktopQuery2 * This,
            /* [in] */ BSTR name,
            /* [retval][out] */ VARIANT *value);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_query )( 
            IGoogleDesktopQuery2 * This,
            /* [retval][out] */ BSTR *ret);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Execute )( 
            IGoogleDesktopQuery2 * This,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *PrepareResultSet )( 
            IGoogleDesktopQuery2 * This,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results);
        
        END_INTERFACE
    } IGoogleDesktopQuery2Vtbl;

    interface IGoogleDesktopQuery2
    {
        CONST_VTBL struct IGoogleDesktopQuery2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQuery2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQuery2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQuery2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQuery2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQuery2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQuery2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQuery2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQuery2_SetOption(This,name,value)	\
    ( (This)->lpVtbl -> SetOption(This,name,value) ) 

#define IGoogleDesktopQuery2_GetOption(This,name,value)	\
    ( (This)->lpVtbl -> GetOption(This,name,value) ) 

#define IGoogleDesktopQuery2_get_query(This,ret)	\
    ( (This)->lpVtbl -> get_query(This,ret) ) 

#define IGoogleDesktopQuery2_Execute(This,results)	\
    ( (This)->lpVtbl -> Execute(This,results) ) 


#define IGoogleDesktopQuery2_PrepareResultSet(This,results)	\
    ( (This)->lpVtbl -> PrepareResultSet(This,results) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQuery2_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryAPI_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryAPI */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryAPI;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryAPI : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Query( 
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryEx( 
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQuery **q) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryAPIVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryAPI * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryAPI * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryAPI * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Query )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryEx )( 
            IGoogleDesktopQueryAPI * This,
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQuery **q);
        
        END_INTERFACE
    } IGoogleDesktopQueryAPIVtbl;

    interface IGoogleDesktopQueryAPI
    {
        CONST_VTBL struct IGoogleDesktopQueryAPIVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryAPI_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryAPI_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryAPI_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryAPI_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryAPI_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryAPI_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryAPI_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryAPI_Query(This,cookie,query,category,ranking,results)	\
    ( (This)->lpVtbl -> Query(This,cookie,query,category,ranking,results) ) 

#define IGoogleDesktopQueryAPI_QueryEx(This,cookie,query,category,ranking,q)	\
    ( (This)->lpVtbl -> QueryEx(This,cookie,query,category,ranking,q) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryAPI_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryAPI2_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryAPI2_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryAPI2 */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryAPI2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryAPI2 : public IGoogleDesktopQueryAPI
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryForEvent( 
            /* [in] */ long cookie,
            /* [in] */ VARIANT event,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **item) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryAPI2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryAPI2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryAPI2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Query )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQueryResultSet **results);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryEx )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ long cookie,
            /* [in] */ BSTR query,
            /* [optional][in] */ VARIANT category,
            /* [optional][in] */ VARIANT ranking,
            /* [retval][out] */ IGoogleDesktopQuery **q);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryForEvent )( 
            IGoogleDesktopQueryAPI2 * This,
            /* [in] */ long cookie,
            /* [in] */ VARIANT event,
            /* [retval][out] */ IGoogleDesktopQueryResultItem **item);
        
        END_INTERFACE
    } IGoogleDesktopQueryAPI2Vtbl;

    interface IGoogleDesktopQueryAPI2
    {
        CONST_VTBL struct IGoogleDesktopQueryAPI2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryAPI2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryAPI2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryAPI2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryAPI2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryAPI2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryAPI2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryAPI2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryAPI2_Query(This,cookie,query,category,ranking,results)	\
    ( (This)->lpVtbl -> Query(This,cookie,query,category,ranking,results) ) 

#define IGoogleDesktopQueryAPI2_QueryEx(This,cookie,query,category,ranking,q)	\
    ( (This)->lpVtbl -> QueryEx(This,cookie,query,category,ranking,q) ) 


#define IGoogleDesktopQueryAPI2_QueryForEvent(This,cookie,event,item)	\
    ( (This)->lpVtbl -> QueryForEvent(This,cookie,event,item) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryAPI2_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_GoogleDesktopAPI_0000_0027 */
/* [local] */ 

typedef /* [uuid] */ 
enum GDTopicProfileId
    {	TOPIC_UNKNOWN	= -1,
	TOPIC_BUSINESS	= 0,
	TOPIC_FINANCE	= 1,
	TOPIC_NEWS	= 2,
	TOPIC_KIDS	= 3,
	TOPIC_GAMES	= 4,
	TOPIC_HEALTH	= 5,
	TOPIC_TRAVEL	= 6,
	TOPIC_SCIENCE	= 7,
	TOPIC_SHOPPING	= 8,
	TOPIC_COMPUTERS	= 9,
	TOPIC_TECHNOLOGY	= 10,
	TOPIC_PROGRAMMER	= 11,
	TOPIC_WEBLOGS	= 12,
	TOPIC_SOCIAL	= 13,
	TOPIC_SPORTS	= 14,
	TOPIC_ENTERTAINMENT	= 15,
	TOPIC_MOVIES	= 16,
	TOPIC_TV	= 17,
	TOPIC_CAREERS	= 18,
	TOPIC_WEATHER	= 19,
	TOPIC_REALESTATE	= 20,
	NUMBER_TOPICS	= 21
    } 	GDTopicProfileId;



extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0027_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_GoogleDesktopAPI_0000_0027_v0_0_s_ifspec;

#ifndef __IGoogleDesktopStatus_INTERFACE_DEFINED__
#define __IGoogleDesktopStatus_INTERFACE_DEFINED__

/* interface IGoogleDesktopStatus */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopStatus;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopStatus : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetProperty( 
            /* [in] */ BSTR property_name,
            /* [retval][out] */ VARIANT *value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetProperty( 
            /* [in] */ BSTR property_name,
            /* [in] */ VARIANT value) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopStatusVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopStatus * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopStatus * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopStatus * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopStatus * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopStatus * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopStatus * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopStatus * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopStatus * This,
            /* [in] */ BSTR property_name,
            /* [retval][out] */ VARIANT *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetProperty )( 
            IGoogleDesktopStatus * This,
            /* [in] */ BSTR property_name,
            /* [in] */ VARIANT value);
        
        END_INTERFACE
    } IGoogleDesktopStatusVtbl;

    interface IGoogleDesktopStatus
    {
        CONST_VTBL struct IGoogleDesktopStatusVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopStatus_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopStatus_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopStatus_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopStatus_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopStatus_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopStatus_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopStatus_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopStatus_GetProperty(This,property_name,value)	\
    ( (This)->lpVtbl -> GetProperty(This,property_name,value) ) 

#define IGoogleDesktopStatus_SetProperty(This,property_name,value)	\
    ( (This)->lpVtbl -> SetProperty(This,property_name,value) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopStatus_INTERFACE_DEFINED__ */



#ifndef __GoogleDesktopAPILib_LIBRARY_DEFINED__
#define __GoogleDesktopAPILib_LIBRARY_DEFINED__

/* library GoogleDesktopAPILib */
/* [helpstring][version][uuid] */ 

typedef 
enum EventFlags
    {	EventFlagIndexable	= 0x1,
	EventFlagContextual	= 0x2,
	EventFlagHistorical	= 0x10,
	EventFlagReplace	= 0x2000,
	EventFlagDuplicate	= 0x4000
    } 	EventFlags;




























EXTERN_C const IID LIBID_GoogleDesktopAPILib;

#ifndef __IGoogleDesktopNotifyEvent2_INTERFACE_DEFINED__
#define __IGoogleDesktopNotifyEvent2_INTERFACE_DEFINED__

/* interface IGoogleDesktopNotifyEvent2 */
/* [unique][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopNotifyEvent2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopNotifyEvent2 : public IGoogleDesktopNotifyEvent
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetKeywords( 
            /* [optional][in] */ VARIANT num_keywords,
            /* [optional][in] */ VARIANT include_weight,
            /* [retval][out] */ BSTR *keywords) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopNotifyEvent2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopNotifyEvent2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopNotifyEvent2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [in] */ BSTR property_name,
            /* [retval][out] */ VARIANT *property_value);
        
        /* [helpstring][propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_SchemaName )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [retval][out] */ BSTR *schema_name);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetKeywords )( 
            IGoogleDesktopNotifyEvent2 * This,
            /* [optional][in] */ VARIANT num_keywords,
            /* [optional][in] */ VARIANT include_weight,
            /* [retval][out] */ BSTR *keywords);
        
        END_INTERFACE
    } IGoogleDesktopNotifyEvent2Vtbl;

    interface IGoogleDesktopNotifyEvent2
    {
        CONST_VTBL struct IGoogleDesktopNotifyEvent2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopNotifyEvent2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopNotifyEvent2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopNotifyEvent2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopNotifyEvent2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopNotifyEvent2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopNotifyEvent2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopNotifyEvent2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopNotifyEvent2_GetProperty(This,property_name,property_value)	\
    ( (This)->lpVtbl -> GetProperty(This,property_name,property_value) ) 

#define IGoogleDesktopNotifyEvent2_get_SchemaName(This,schema_name)	\
    ( (This)->lpVtbl -> get_SchemaName(This,schema_name) ) 


#define IGoogleDesktopNotifyEvent2_GetKeywords(This,num_keywords,include_weight,keywords)	\
    ( (This)->lpVtbl -> GetKeywords(This,num_keywords,include_weight,keywords) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopNotifyEvent2_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem2_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryResultItem2_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryResultItem2 */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryResultItem2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryResultItem2 : public IGoogleDesktopQueryResultItem
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetKeywords( 
            /* [optional][in] */ VARIANT num_keywords,
            /* [optional][in] */ VARIANT include_weight,
            /* [retval][out] */ BSTR *keywords) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSnippet( 
            /* [optional][in] */ VARIANT max_chars,
            /* [optional][in] */ VARIANT words_to_hilite,
            /* [retval][out] */ BSTR *snippet) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryResultItem2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryResultItem2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryResultItem2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ BSTR prop_name,
            /* [retval][out] */ VARIANT *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFromIndex )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [in] */ VARIANT_BOOL remove_all_related_versions);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_schema )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [retval][out] */ BSTR *schema_name);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_versions )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [retval][out] */ IGoogleDesktopDocumentVersions **ret);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetKeywords )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [optional][in] */ VARIANT num_keywords,
            /* [optional][in] */ VARIANT include_weight,
            /* [retval][out] */ BSTR *keywords);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSnippet )( 
            IGoogleDesktopQueryResultItem2 * This,
            /* [optional][in] */ VARIANT max_chars,
            /* [optional][in] */ VARIANT words_to_hilite,
            /* [retval][out] */ BSTR *snippet);
        
        END_INTERFACE
    } IGoogleDesktopQueryResultItem2Vtbl;

    interface IGoogleDesktopQueryResultItem2
    {
        CONST_VTBL struct IGoogleDesktopQueryResultItem2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryResultItem2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryResultItem2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryResultItem2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryResultItem2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryResultItem2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryResultItem2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryResultItem2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryResultItem2_GetProperty(This,prop_name,value)	\
    ( (This)->lpVtbl -> GetProperty(This,prop_name,value) ) 

#define IGoogleDesktopQueryResultItem2_RemoveFromIndex(This,remove_all_related_versions)	\
    ( (This)->lpVtbl -> RemoveFromIndex(This,remove_all_related_versions) ) 

#define IGoogleDesktopQueryResultItem2_get_schema(This,schema_name)	\
    ( (This)->lpVtbl -> get_schema(This,schema_name) ) 

#define IGoogleDesktopQueryResultItem2_get_versions(This,ret)	\
    ( (This)->lpVtbl -> get_versions(This,ret) ) 


#define IGoogleDesktopQueryResultItem2_GetKeywords(This,num_keywords,include_weight,keywords)	\
    ( (This)->lpVtbl -> GetKeywords(This,num_keywords,include_weight,keywords) ) 

#define IGoogleDesktopQueryResultItem2_GetSnippet(This,max_chars,words_to_hilite,snippet)	\
    ( (This)->lpVtbl -> GetSnippet(This,max_chars,words_to_hilite,snippet) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryResultItem2_INTERFACE_DEFINED__ */


#ifndef __IGoogleDesktopQueryResultItem3_INTERFACE_DEFINED__
#define __IGoogleDesktopQueryResultItem3_INTERFACE_DEFINED__

/* interface IGoogleDesktopQueryResultItem3 */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGoogleDesktopQueryResultItem3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
     struct 
    IGoogleDesktopQueryResultItem3 : public IGoogleDesktopQueryResultItem2
    {
    public:
        virtual /* [propput][id] */ HRESULT STDMETHODCALLTYPE put_onkeywordsready( 
            /* [in] */ VARIANT callback) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Open( 
            /* [optional][in] */ VARIANT action_id) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetPropertyIterator( 
            /* [retval][out] */ IGoogleDesktopItemPropertyIterator **prop_iterator) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGoogleDesktopQueryResultItem3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGoogleDesktopQueryResultItem3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGoogleDesktopQueryResultItem3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetProperty )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ BSTR prop_name,
            /* [retval][out] */ VARIANT *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveFromIndex )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ VARIANT_BOOL remove_all_related_versions);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_schema )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [retval][out] */ BSTR *schema_name);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_versions )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [retval][out] */ IGoogleDesktopDocumentVersions **ret);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetKeywords )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [optional][in] */ VARIANT num_keywords,
            /* [optional][in] */ VARIANT include_weight,
            /* [retval][out] */ BSTR *keywords);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSnippet )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [optional][in] */ VARIANT max_chars,
            /* [optional][in] */ VARIANT words_to_hilite,
            /* [retval][out] */ BSTR *snippet);
        
        /* [propput][id] */ HRESULT ( STDMETHODCALLTYPE *put_onkeywordsready )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [in] */ VARIANT callback);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Open )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [optional][in] */ VARIANT action_id);
        
        HRESULT ( STDMETHODCALLTYPE *GetPropertyIterator )( 
            IGoogleDesktopQueryResultItem3 * This,
            /* [retval][out] */ IGoogleDesktopItemPropertyIterator **prop_iterator);
        
        END_INTERFACE
    } IGoogleDesktopQueryResultItem3Vtbl;

    interface IGoogleDesktopQueryResultItem3
    {
        CONST_VTBL struct IGoogleDesktopQueryResultItem3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGoogleDesktopQueryResultItem3_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGoogleDesktopQueryResultItem3_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGoogleDesktopQueryResultItem3_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGoogleDesktopQueryResultItem3_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGoogleDesktopQueryResultItem3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGoogleDesktopQueryResultItem3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGoogleDesktopQueryResultItem3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGoogleDesktopQueryResultItem3_GetProperty(This,prop_name,value)	\
    ( (This)->lpVtbl -> GetProperty(This,prop_name,value) ) 

#define IGoogleDesktopQueryResultItem3_RemoveFromIndex(This,remove_all_related_versions)	\
    ( (This)->lpVtbl -> RemoveFromIndex(This,remove_all_related_versions) ) 

#define IGoogleDesktopQueryResultItem3_get_schema(This,schema_name)	\
    ( (This)->lpVtbl -> get_schema(This,schema_name) ) 

#define IGoogleDesktopQueryResultItem3_get_versions(This,ret)	\
    ( (This)->lpVtbl -> get_versions(This,ret) ) 


#define IGoogleDesktopQueryResultItem3_GetKeywords(This,num_keywords,include_weight,keywords)	\
    ( (This)->lpVtbl -> GetKeywords(This,num_keywords,include_weight,keywords) ) 

#define IGoogleDesktopQueryResultItem3_GetSnippet(This,max_chars,words_to_hilite,snippet)	\
    ( (This)->lpVtbl -> GetSnippet(This,max_chars,words_to_hilite,snippet) ) 


#define IGoogleDesktopQueryResultItem3_put_onkeywordsready(This,callback)	\
    ( (This)->lpVtbl -> put_onkeywordsready(This,callback) ) 

#define IGoogleDesktopQueryResultItem3_Open(This,action_id)	\
    ( (This)->lpVtbl -> Open(This,action_id) ) 

#define IGoogleDesktopQueryResultItem3_GetPropertyIterator(This,prop_iterator)	\
    ( (This)->lpVtbl -> GetPropertyIterator(This,prop_iterator) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGoogleDesktopQueryResultItem3_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_GoogleDesktop;

#ifdef __cplusplus

class DECLSPEC_UUID("579822B3-44CD-4786-83E0-AE32BCB9E6B1")
GoogleDesktop;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopEventPublisher;

#ifdef __cplusplus

class DECLSPEC_UUID("2C6F11D4-CF22-4e1f-A271-2A4A0393ADAC")
GoogleDesktopEventPublisher;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopIndexingComponentRegister;

#ifdef __cplusplus

class DECLSPEC_UUID("BB8B07A0-B8D1-44e0-A262-C9B7212AEC68")
GoogleDesktopIndexingComponentRegister;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopRegistrar;

#ifdef __cplusplus

class DECLSPEC_UUID("AC129136-EB1C-4fff-B0A2-6D6761BE4138")
GoogleDesktopRegistrar;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopSchemaFilter;

#ifdef __cplusplus

class DECLSPEC_UUID("1B4C0C56-5990-4277-826E-1508037DD8A7")
GoogleDesktopSchemaFilter;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopSchemaPropertyFilter;

#ifdef __cplusplus

class DECLSPEC_UUID("9130995A-B2F2-47c7-BD60-BC02E950A8A8")
GoogleDesktopSchemaPropertyFilter;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopContextualFilter;

#ifdef __cplusplus

class DECLSPEC_UUID("81ACBD30-3750-4c4e-BDA1-173FB509D475")
GoogleDesktopContextualFilter;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopFilterCollection;

#ifdef __cplusplus

class DECLSPEC_UUID("2B62A832-2CA2-4843-86CA-45450D35EADA")
GoogleDesktopFilterCollection;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopQueryAPI;

#ifdef __cplusplus

class DECLSPEC_UUID("4E26495C-CA41-4085-96C4-8CA805AF297E")
GoogleDesktopQueryAPI;
#endif

EXTERN_C const CLSID CLSID_GoogleDesktopStatus;

#ifdef __cplusplus

class DECLSPEC_UUID("75B02994-EF97-421C-AACD-28DAC318129B")
GoogleDesktopStatus;
#endif
#endif /* __GoogleDesktopAPILib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  LPSAFEARRAY_UserSize(     unsigned long *, unsigned long            , LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserMarshal(  unsigned long *, unsigned char *, LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserUnmarshal(unsigned long *, unsigned char *, LPSAFEARRAY * ); 
void                      __RPC_USER  LPSAFEARRAY_UserFree(     unsigned long *, LPSAFEARRAY * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


