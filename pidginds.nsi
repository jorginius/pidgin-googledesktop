; Script based on the Skype4Pidgin and Off-the-Record Messaging NSI files


SetCompress auto
SetCompressor lzma

InstallDir "$PROGRAMFILES\PidginDS"

; todo: SetBrandingImage
; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "pidgin-googledesktop"
!define PRODUCT_VERSION "0.6"
!define PRODUCT_PUBLISHER "Jorge Rodriguez"
!define PRODUCT_WEB_SITE "http://libertonia.escomposlinux.org/Diary/jorginius"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "COPYING"
; Instfiles page
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Spanish"

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "${PRODUCT_NAME}-${PRODUCT_VERSION}.exe"

Var "PidginDir"

ShowInstDetails show
ShowUnInstDetails show

Function .onInit
	;Language selection dialog

	Push ""
	Push ${LANG_SPANISH}
	Push Spanish
	Push ${LANG_ENGLISH}
	Push English
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
		Abort
FunctionEnd

Section "MainSection" SEC01
    ;Check for GDS installation
    Call IsGoogleDesktop

    ;Check for pidgin installation
    Call GetPidginInstPath
    
    SetOverwrite try
    
	SetOutPath $INSTDIR
	File "register.exe"
	File /r "register.mo"

    SetOverwrite try
	copy:
		ClearErrors

		Delete "$PidginDir\plugins\gds_plugin.dll"
		Delete "$PidginDir\locale\es\LC_MESSAGES\gds_plugin.mo"

                IfErrors dllbusy
		SetOutPath "$PidginDir\plugins"
                File "gds_plugin.dll"
                SetOutPath "$PidginDir\locale\es\LC_MESSAGES"
                File "gds_locale\es_ES\LC_MESSAGES\gds_plugin.mo"
		Goto after_copy
	dllbusy:
		MessageBox MB_RETRYCANCEL "gds_plugin.dll is busy. Please close Pidgin (including tray icon) and try again" IDCANCEL cancel
		Goto copy
	cancel:
		Abort "Installation of pidgin-googledesktop aborted"
	after_copy:
        	ExecShell "" "$INSTDIR\register.exe" "-i" SW_HIDE

     WriteUninstaller $INSTDIR\Uninst.exe
SectionEnd

Section "Uninstall"
  Delete $INSTDIR\Uninst.exe
  ;ExecShell "" "$INSTDIR\register.exe" "-u" SW_HIDE
  ExecWait "$INSTDIR\register.exe -u" $0

  Delete $INSTDIR\register.exe

  RMDir /r $INSTDIR
    
  ReadRegStr $PidginDir HKLM "Software\pidgin" ""

      redo:
                ClearErrors
                Delete "$PidginDir\plugins\gds_plugin.dll"
                Delete "$PidginDir\locale\es_ES\LC_MESSAGES\gds_plugin.mo"
                IfErrors dllbusy after_delete
      dllbusy:
		MessageBox MB_RETRYCANCEL "gds_plugin.dll is busy. Please close Pidgin (including tray icon) and try again"
		Goto redo
      after_delete:
                
SectionEnd

Function IsGoogleDesktop
  Push $0
  ReadRegStr $0 HKLM "Software\Google\Google Desktop" "install_dir"
  ;The error flag will be set and $x will be set to an empty string ("")
  IfErrors 0 cont
           MessageBox MB_OK|MB_ICONINFORMATION "Failed to find Google Desktop."
           Abort "Failed to find Google Desktop installation."
  cont:
FunctionEnd


Function GetPidginInstPath
  Push $0
  ReadRegStr $0 HKLM "Software\pidgin" ""
	IfFileExists "$0\pidgin.exe" cont
	ReadRegStr $0 HKCU "Software\pidgin" ""
	IfFileExists "$0\pidgin.exe" cont
		MessageBox MB_OK|MB_ICONINFORMATION "Failed to find Pidgin installation."
		Abort "Failed to find Pidgin installation. Please install Pidgin first."
  cont:
	StrCpy $PidginDir $0
FunctionEnd
